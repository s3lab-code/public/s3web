all:

# Just a big hammer
install:
	@if test ! -f docker/.env; then \
	    echo "ERROR! No .env in docker - you should make one!"; \
		exit 1; \
	fi
	@cd docker && docker compose up -d && cd -
	@if test ! -f lasrweb/config.inc; then \
	    echo "WARNING! No config.inc in lasrweb - you should make one!"; \
	fi
	@if test ! -f lasrweb/.htaccess; then \
	    echo "WARNING! No .htaccess in lasrweb - you should make one!"; \
	fi
	@docker container inspect s3web-phpmyadmin >/dev/null 2>&1 && \
	    echo "WARNING! phpmyadmin container is running publicly - consider stopping it (`make stop-pma`)!"

uninstall:
	@if test ! -f docker/.env; then \
	    echo "ERROR! No .env in docker - you should make one!"; \
		exit 1; \
	fi
	@cd docker && docker compose down && cd -

reinstall:
	@if test ! -f docker/.env; then \
	    echo "ERROR! No .env in docker - you should make one!"; \
		exit 1; \
	fi
	@cd docker && docker compose down && docker compose build && docker compose up -d && cd -

logs:
	@cd docker && docker compose logs

start-pma:
	@cd docker && docker compose start phpmyadmin

stop-pma:
	@cd docker && docker compose stop phpmyadmin

force-cert-renew:
	@cd docker && docker compose exec cert /app/force_renew

check-cert-status:
	@cd docker && docker compose exec cert /app/cert_status
