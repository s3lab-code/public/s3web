# TODO

* Sort sponsors based on funding amount.

* Add a logout button in the internal page

* Guard every "delete" button with a warning message box.

* Make alumni status a separate checkbox.

* Make affiliated status a separate checkbox?
  * E.g., grad student we work with, but who is not "part of" S3 Lab
