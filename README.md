REQUIREMENTS
============
LAMP: Linux, Apache, MySQL, PHP


SETUP
=====
1. Setup a database at host X, name the database Y, and create 2 users: A and
   B.  A represents any public (non-administrative) user; A has read-only
   access to the site.  B represents any administrator on the site and is
   granted full access.  The username and password for B are the credentials
   for modifying content on the site (we only have one shared account for all
   administrators).

   Make a config.inc file by copying config.inc.example and filling in the
   values appropriately. Use the information for user A in this file.

2. Generate the schema for your database by importing the schema.sql (attached)
   through MySQL's commands.

3. Edit the .htaccess file, replacing URLs and paths with the ones used for
   your site. mod_rewrite is required.

4. Move the directory lasrweb in the path where you want to setup the site. 
   You can use the provided makefile to automat this; make sure to edit the
   path. The makefile purposely doesn't copy over configuration files, to
   make it easier to maintain seperate live and development trees.

5. Replace any LASR-specific content (e.g., pictures, contact info) with your
   own.

6. Enjoy, and spread the word.


MAINTENANCE AND USE
===================
In order to effectively use the website, you will need to add content about
researchers, publications, etc.  Do this by pointing your favorite web browser
to:

    <URL of site>/internal

Enter B's credentials.  From that point, follow the instructions presented on
the screen. It is recommended to create papers by pasting a BibTeX entry into
the box on the paper screen, which greatly eases the process.

Only give the credentials of B to "trusted" people, as knowledge of B's
credentials effectively gives them unrestricted access to the database.  That
said, one of the design goals that we strived for was to crowdsource the
generation of content so that no one person does all the dirty work.  Thus,
enough people should have access to B's credentials to make the site useful. 

Enjoy!


DOCKERIZATION
=============
* Install Docker and Docker Compose.
* Copy `docker/sample.env` to `docker/.env`.
* Update `docker/.env` with your own research group's information
  (domain and subdomain names, email address, MySQL passwords).
* Create the subdomains in the DNS records of the domain.
* Run `make install` to start the Docker comtainers.
* Follow [SETUP](#setup) to set up the LASR-based website
  (with the default subdomain of `www`).
* Use MyPhPAdmin at `http://<domain>:8080` to set up the database more
  conveniently.
* When the database setup is complete, run `make stop-pma` to stop the
  MyPHPAdmin container.
* If you want to start the MyPHPAdmin container again, run
  `make start-pma`.
* Make sure to stop the MyPHPAdmin container after use since it is
  publicly accessible from the web.
* If necessary, force certificates renewal by running
  `make force-cert-renew`.
* To check the status of the certificates, run
  `make check-cert-status`.


CREDITS
=======
Manos Kapritsos, Sangmin Lee, and Edmund L. Wong  
Laboratory of Advanced Systems Research  
Department of Computer Science  
The University of Texas at Austin

Robert Ricci  
Flux Research Group  
University of Utah

Chung Hwan Kim  
Software & Systems Security Laboratory  
Department of Computer Science  
The University of Texas at Dallas
