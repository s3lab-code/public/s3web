<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
require "header.inc";
require "tools.inc";

error_reporting(E_ALL);
ini_set('display_errors', 1);

$id = getIDFromParams($db,"Project");

#$result = mysql_query("SELECT * from Project where uid='$id';");
$result = mysqli_query($db,"SELECT * from Project where uid='$id';");

#if($result && mysql_num_rows($result)>0) {
if($result && mysqli_num_rows($result)>0) {
    #$row = mysql_fetch_assoc($result);
    $row = mysqli_fetch_assoc($result);
} else {
	die("Invalid unique id");
}

SPITHEADER("$row[Name]");

?>

<body>
<div id="project-page" class="page-container">
<div id="listing-page" class="page-container">

<?php require "title.inc"; ?>

<?php SPITMENU("projects"); ?>

<div id="profile-box" class="box-shadow projects-listing">
<div class="transparent-box">

<?php if (isset($row['Picture'])) {
    echo "<img src=\"download?uid=$row[uid]&amp;logo=1\" class=\"projlogo-large\" alt=\"$row[Name] logo\">\n";
} ?>


<p class="section-title">
<?php echo $row['Name']; ?>
</p>

<div id="project-details">
<?php 
echo $row['Details'];
?>
</div>

<div class="clear"></div>
</div>
</div>

<div class="box-shadow people-listing">


<?php
$titletext = "people";
if(projectHasAlumni($db,$id)) { $titletext = "current " . $titletext; }
    peopleList($db,array("projectID" => $id, "includeAffil" => 1, 
        "projectHack" => 1, "firstHeader" => $titletext));  

if (projectHasAlumni($db,$id)) {
    peopleList($db,array("projectID" => $id, "alumniOnly" => 1, "firstHeader" => "alumni"));
}
?>

<div style="clear:both;"> </div>

</div>

<?php if (projectHasPapers($db,$id)) { ?>

<div class="box-shadow papers-listing">
<p class="section-title">publications</p> 
<?php getPublicationsModule($db,array('projectID' => $id)); ?>
</div>

<?php } ?>

<?php if (projectHasSponsors($db,$id)) { ?>

<div class="box-shadow sponsors-listing">
<p class="section-title">sponsors</p> 
<?php
#$result = mysql_query("SELECT SponsorID FROM RelProjectSponsor WHERE ProjectID='$id'");
#$result = mysqli_query($db,"SELECT SponsorID FROM RelProjectSponsor WHERE ProjectID='$id' ORDER BY SponsorID ASC");
$result = mysqli_query($db,"SELECT SponsorID FROM RelProjectSponsor WHERE ProjectID='".mysqli_real_escape_string($db, $id)."' ORDER BY SponsorID ASC");
#while ($row = mysql_fetch_assoc($result)) {
while ($row = mysqli_fetch_assoc($result)) {
    showSponsorProfile($db,$row['SponsorID']);
}
?>
</div>

<?php } ?>

</div>
</div>

</body>
</html>


