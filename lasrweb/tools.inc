<?php

function getPosition($poskey) {
    $posmap = array("faculty" => "Faculty", 
                    "affilfaculty" => "Affiliated Faculty", 
                    "scientist" => "Research staff", 
                    "postdoc" => "Post-doctoral associate", 
                    "visitor" => "Visiting scholar",
                    "phd" => "PhD student", 
                    "masters" => "Masters student", 
                    "undergrad" => "Undergraduate student", 
                    "staff" => "Staff",
                    "alum" => "Alum",
                    "ext" => "External collaborator",
                    "other" => "Other");
    return $posmap[$poskey];
}

function printErrorFrame($errorMsg) {
	echo "<div style=\"background-color:#FF3333; padding: 2px 2px 2px 10px;color:white;font-weight: bold; clear:both;\">".
		$errorMsg.
		"</div>\n"; 
}

function printSuccessFrame($msg) {
	echo "<div style=\"background-color:green; padding: 2px 2px 2px 10px;color:white;font-weight: bold; clear:both;\">".
		$msg.
		"</div>\n"; 
}

function getMonthText($month_num)
{
  if( ! ($month_num >= 1 && $month_num <= 12)){
      return "";
  }
  $MonthName = array(1=>'January','February','March','April','May','June','July','August',
                      'September','October','November','December');
  return $MonthName[$month_num];
}

function startsWith($haystack, $needle, $case)
{
        $length = strlen($needle);
        if($case){
          return strncasecmp($haystack, $needle, $length) == 0;
        } else {
          return strncmp($haystack, $needle, $length) == 0;
        }
}

function getNewsModule($db, $limits = array()) {

    $select_clause = "SELECT N.Date, N.Description " .
        "FROM News as N ";
    $order_clause = "ORDER BY Date DESC, uid DESC";

  if(isset($limits['limit'])){
    #$limit_clause = "LIMIT $limits[limit]";
    $limit_clause = "LIMIT ".mysqli_real_escape_string($db, $limits['limit']);
  } else {
      $limit_clause = "";
  }

  $query = $select_clause . " " . $order_clause . " " . $limit_clause . ";";

  #$result = mysql_query($query);
  $result = mysqli_query($db,$query);

  #if(!$result || mysql_num_rows($result) == 0) {
  if(!$result || mysqli_num_rows($result) == 0) {
  ?>
  no news
  <?php
	return;
  }

  # Silly heuristic to decide if dividing by years makes sense
  #if (mysql_num_rows($result) <= 4) {
  if (mysqli_num_rows($result) <= 4) {
      $year_breaks = 0;
      echo "<div class=\"news-group\">\n";
  } else {
      $year_breaks = 1;
  } 

  $previous_year = -1;
  #while($news = mysql_fetch_assoc($result)) {
  while($news = mysqli_fetch_assoc($result)) {
      $year = date("Y", strtotime($news["Date"]));
      if ($previous_year != $year && $year_breaks) {
          if ($previous_year != -1) {
              echo "</div>";
          }
          echo "<div class=\"yearbreak\">$year</div>\n";
          echo "<div class=\"news-group\">\n";
          $previous_year = $year;
      }
  ?>
  <div class="news">
  <?php if (!isset($limits['excludeDates'])) {?>
  <div class="date">
  <?php echo date("m/d/Y", strtotime($news["Date"]));?></div>
  <?php
  }
  $description = $news["Description"];
  if(startsWith($description, "<p>", true)){
      $description = substr($description, 3, -4);
  }
  echo $description;?></div>
  <?php
  }
  echo "</div>\n";
}

function getPersonAreaTable($db, $personID) {
	$table = array(array());
	#$result = mysql_query("SELECT uid, Name FROM ResearchArea");
	$result = mysqli_query($db,"SELECT uid, Name FROM ResearchArea");
	$count = 0;
	#while($area = mysql_fetch_assoc($result)) {
	while($area = mysqli_fetch_assoc($result)) {
		$table[$count%3][$count/3] = $area;
		$count++;
	}
	$areas = array();
	#$res = mysql_query("SELECT AreaID FROM RelPersonArea WHERE PersonID=$personID");
	#$res = mysqli_query($db,"SELECT AreaID FROM RelPersonArea WHERE PersonID=$personID");
	$res = mysqli_query($db,"SELECT AreaID FROM RelPersonArea WHERE PersonID=".mysqli_real_escape_string($db, $personID));
	#while($aid = mysql_fetch_assoc($res)) {
	while($aid = mysqli_fetch_assoc($res)) {
		array_push($areas, $aid["AreaID"]);
	}
	echo "<table>\n";
	echo "<tr>\n";
	for($i=0;$i<count($table);$i++) {
		echo "<td width=\"200\">\n";
		for($j=0;$j<count($table[$i]);$j++) {
			$area = $table[$i][$j];
			if(in_array($area["uid"],$areas)) {
				echo "<input type=\"checkbox\" name=\"area".$area["uid"]."\" value=\"1\" checked>".$area["Name"]."<br>\n";
			} else {
				echo "<input type=\"checkbox\" name=\"area".$area["uid"]."\" value=\"0\">".$area["Name"]."<br>\n";
		}
		}
		echo "</td>\n";
	}
	echo "</tr>\n";	
	echo "</table>\n";	
}


function getAreaTable($db, $ID, $type="Paper") {
	$table = array(array());
	#$result = mysql_query("SELECT uid, Name FROM ResearchArea");
	$result = mysqli_query($db,"SELECT uid, Name FROM ResearchArea");
	$count = 0;
	#while($area = mysql_fetch_assoc($result)) {
	while($area = mysqli_fetch_assoc($result)) {
		$table[$count%3][$count/3] = $area;
		$count++;
	}
	$areas = array();
        # silly hack
        if (!$ID) {
            $ID = -1;
        }
	#$res = mysql_query("SELECT AID FROM Rel${type}Area WHERE ${type}ID=$ID");
	#$res = mysqli_query($db,"SELECT AID FROM Rel${type}Area WHERE ${type}ID=$ID");
	$res = mysqli_query($db,"SELECT AID FROM Rel${type}Area WHERE ${type}ID=".mysqli_real_escape_string($db, $ID));
	#while($aid = mysql_fetch_assoc($res)) {
	while($aid = mysqli_fetch_assoc($res)) {
		array_push($areas, $aid["AID"]);
	}
	echo "<table>\n";
	echo "<tr>\n";
	for($i=0;$i<count($table);$i++) {
		echo "<td width=\"200\">\n";
		for($j=0;$j<count($table[$i]);$j++) {
			$area = $table[$i][$j];
			if(in_array($area["uid"],$areas)) {
				echo "<input type=\"checkbox\" name=\"area".$area["uid"]."\" value=\"1\" checked>".$area["Name"]."<br>\n";
			} else {
				echo "<input type=\"checkbox\" name=\"area".$area["uid"]."\" value=\"0\">".$area["Name"]."<br>\n";
		}
		}
		echo "</td>\n";
	}
	echo "</tr>\n";	
	echo "</table>\n";	
}

function getProjTable($db, $ID, $type="Paper") {
	$table = array(array());
	#$result = mysql_query("SELECT uid, Name FROM Project");
	$result = mysqli_query($db,"SELECT uid, Name FROM Project");
	$count = 0;
	#while($project = mysql_fetch_assoc($result)) {
	while($project = mysqli_fetch_assoc($result)) {
		$table[$count%3][$count/3] = $project;
		$count++;
	}
	$projects = array();
	#$res = mysql_query("SELECT ProjectID FROM RelProject${type} WHERE ${type}ID=$ID");
	#$res = mysqli_query($db,"SELECT ProjectID FROM RelProject${type} WHERE ${type}ID=$ID");
	$res = mysqli_query($db,"SELECT ProjectID FROM RelProject${type} WHERE ${type}ID=".mysqli_real_escape_string($db, $ID));
	#while($pid = mysql_fetch_assoc($res)) {
	while($pid = mysqli_fetch_assoc($res)) {
		array_push($projects, $pid["ProjectID"]);
	}
	echo "<table>\n";
	echo "<tr>\n";
	for($i=0;$i<count($table);$i++) {
		echo "<td width=\"200\">\n";
		for($j=0;$j<count($table[$i]);$j++) {
			$project = $table[$i][$j];
			if(in_array($project["uid"],$projects)) {
				echo "<input type=\"checkbox\" name=\"project".$project["uid"]."\" value=\"1\" checked>".$project["Name"]."<br>\n";
			} else {
				echo "<input type=\"checkbox\" name=\"project".$project["uid"]."\" value=\"0\">".$project["Name"]."<br>\n";
		}
		}
		echo "</td>\n";
	}
	echo "</tr>\n";	
	echo "</table>\n";	
}

function getCourseTable($db, $ID, $type="Person") {
	$table = array(array());
	#$result = mysql_query("SELECT uid, Name FROM Course");
	$result = mysqli_query($db,"SELECT uid, Name FROM Course");
	$count = 0;
	#while($course = mysql_fetch_assoc($result)) {
	while($course = mysqli_fetch_assoc($result)) {
		$table[$count%3][$count/3] = $course;
		$count++;
	}
	$courses = array();
	#$res = mysql_query("SELECT CourseID FROM RelCourse${type} WHERE ${type}ID=$ID");
	#$res = mysqli_query($db,"SELECT CourseID FROM RelCourse${type} WHERE ${type}ID=$ID");
	$res = mysqli_query($db,"SELECT CourseID FROM RelCourse${type} WHERE ${type}ID=".mysqli_real_escape_string($db, $ID));
	#while($pid = mysql_fetch_assoc($res)) {
	while($pid = mysqli_fetch_assoc($res)) {
		array_push($courses, $pid["CourseID"]);
	}
	echo "<table>\n";
	echo "<tr>\n";
	for($i=0;$i<count($table);$i++) {
		echo "<td width=\"400\">\n";
		for($j=0;$j<count($table[$i]);$j++) {
			$course = $table[$i][$j];
			if(in_array($course["uid"],$courses)) {
				echo "<input type=\"checkbox\" name=\"course".$course["uid"]."\" value=\"1\" checked>".$course["Name"]."<br>\n";
			} else {
				echo "<input type=\"checkbox\" name=\"course".$course["uid"]."\" value=\"0\">".$course["Name"]."<br>\n";
		}
		}
		echo "</td>\n";
	}
	echo "</tr>\n";	
	echo "</table>\n";	
}

function getAuthorDropdown($db, $elementID, $authorID, $authorText) {
	// do some preprocessing to find duplicate names
	$table = array();
	$list = array();
	#$result = mysql_query("SELECT uid,Full_name,Email FROM Person");
	$result = mysqli_query($db,"SELECT uid,Full_name,Email FROM Person");
	#while($person = mysql_fetch_assoc($result)) {
	while($person = mysqli_fetch_assoc($result)) {
                $uid = $person["uid"];
                $name = $person["Full_name"];
		if(!array_key_exists($name, $table)) {
			$table[$name] = $uid;
		} else {
			array_push($list, $table[$name]);	// push the existing
			array_push($list, $uid);		// push the new
		}
	}
	
	echo "<span id=\"spanauthor".$elementID."\"";
	if(($authorID == "" && $authorText == "") && $elementID > 0) {
		echo " style=\"display:none;\">\n";
		//echo " style=\"visibility:hidden;position:absolute;\">\n";
	} else {
		echo " style=\"display:block;\">\n";
		//echo " style=\"visibility:visible;\">\n";
	}
	echo "<select name=\"author".$elementID."\">\n";
	echo "  <option value=\"\">Select author</option>\n";
	echo "  <option value=\"\">---FACULTY---</option>\n";
	#$result = mysql_query("SELECT uid,Full_name,Email FROM Person WHERE Title='faculty' OR title='affilfaculty' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1)");
	$result = mysqli_query($db,"SELECT uid,Full_name,Email FROM Person WHERE Title='faculty' OR title='affilfaculty' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1)");
	#while($person = mysql_fetch_assoc($result)) {
	while($person = mysqli_fetch_assoc($result)) {
		$uid = $person["uid"];
		$name = $person["Full_name"];
		$email = $person["Email"];
		echo "  <option value=\"".$uid."\"";
		if($uid == $authorID) {
			echo " selected";
		}
		if (in_array($uid, $list)) {
			echo ">".$name." (".$email.")</option>\n";
		} else {
			echo ">".$name."</option>\n";
		}
	}
	#$result = mysql_query("SELECT uid,Full_name,Email FROM Person WHERE Title='phd' OR Title='masters' OR Title='undergrad' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1)");
	$result = mysqli_query($db,"SELECT uid,Full_name,Email FROM Person WHERE Title='phd' OR Title='masters' OR Title='undergrad' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1)");
	echo "  <option value=\"\">---STUDENTS---</option>\n";
        #while($person = mysql_fetch_assoc($result)) {
        while($person = mysqli_fetch_assoc($result)) {
                $uid = $person["uid"];
                $name = $person["Full_name"];
                $email = $person["Email"];
                echo "  <option value=\"".$uid."\"";
                if($uid == $authorID) {
                        echo " selected";
                }
		if (in_array($uid, $list)) {
                        echo ">".$name." (".$email.")</option>\n";
                } else {
                        echo ">".$name."</option>\n";
                }
        }
	#$result = mysql_query("SELECT uid,Full_name,Email FROM Person WHERE Title='scientist' OR Title='alum' OR Title='other' OR Title='postdoc' OR Title='visitor' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1)");
	$result = mysqli_query($db,"SELECT uid,Full_name,Email FROM Person WHERE Title='scientist' OR Title='alum' OR Title='other' OR Title='postdoc' OR Title='visitor' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1)");
        echo "  <option value=\"\">---OTHER---</option>\n";
        #while($person = mysql_fetch_assoc($result)) {
        while($person = mysqli_fetch_assoc($result)) {
                $uid = $person["uid"];
                $name = $person["Full_name"];
                $email = $person["Email"];
                echo "  <option value=\"".$uid."\"";
                if($uid == $authorID) {
                        echo " selected";
                }
		if (in_array($uid, $list)) {
                        echo ">".$name." (".$email.")</option>\n";
                } else {
                        echo ">".$name."</option>\n";
                }
        }
	echo "</select>";
	//echo "<span class=\"handle\" style=\"padding-left:15px;\">move</span>";
        echo "OR <input type=\"text\" name=\"authorText$elementID\"";
        if (isset($authorText)) {
            echo " value=\"$authorText\"";
        }
        echo ">";
	echo "<br></span>";
}

function getAuthorIDs($authorList) {
  $authorIDs = explode(",",$authorList);
  return $authorIDs;
}

function getAuthorList($numAuthors,$authorIDs,$authorTexts) {
  $authorList = "";
  for($i=0;$i < $numAuthors; $i++) {
    if($i != 0) {
	$authorList .= ",";
    }
    # Numeric IDs take precedence
    if (isset($authorIDs[$i]) && is_numeric($authorIDs[$i])) {
        $authorList .= $authorIDs[$i];
    } else {
        $authorList .= $authorTexts[$i];
    }
  }
  return $authorList;
}

function getPublications($db, $uid) {
  getPublicationsModule($db, $uid?array("uid" => $uid):array());
}  

function getPublicationsModule($db, $limits = array()) {

    $select_clause = "SELECT P.Title, P.AuthorList, P.Venue_full, P.Venue_short, P.Pdf IS NOT NULL as Pdf, P.slides IS NOT NULL as slides, P.slides_type, P.uid, P.AwardPaper, P.Type, P.Year, P.Month, P.VolumeNum, P.Number, P.Institution, P.ArtType, P.Web, P.Code " .
        "FROM Paper as P ";
    $where_clauses = array("1");
    $order_clause = "ORDER BY Year DESC, Month DESC";

    if(isset($limits['uid'])) {
        $select_clause .= "LEFT JOIN RelPaperAuthor as RA on P.uid = RA.PaperID ";
        #array_push($where_clauses,"RA.AuthorID=$limits[uid]");
        array_push($where_clauses,"RA.AuthorID=".mysqli_real_escape_string($db, $limits['uid']));
    }
    if (isset($limits['areaID'])) {
        $select_clause .= "LEFT JOIN RelPaperArea as RB on P.uid=RB.PaperID ";
        #array_push($where_clauses,"RB.AID=$limits[areaID]");
        array_push($where_clauses,"RB.AID=".mysqli_real_escape_string($db, $limits['areaID']));
    }
    if (isset($limits['projectID'])) {
        $select_clause .= "LEFT JOIN RelProjectPaper as RC on P.uid=RC.PaperID ";
        #array_push($where_clauses,"RC.ProjectID=$limits[projectID]");
        array_push($where_clauses,"RC.ProjectID=".mysqli_real_escape_string($db,$limits['projectID']));
    }

    if (isset($limits['excludeTheses'])) {
        array_push($where_clauses,"P.Type!='MSThesis'");
        array_push($where_clauses,"P.Type!='PhDThesis'");
    }

    if (isset($limits['excludeTRs'])) {
        array_push($where_clauses,"P.Type!='TR'");
    }


  if(isset($limits['limit'])){
    #$limit_clause = "LIMIT $limits[limit]";
    $limit_clause = "LIMIT ".mysqli_real_escape_string($db, $limits['limit']);
  } else {
      $limit_clause = "";
  }

  $query = $select_clause . " WHERE " . join(" AND ", $where_clauses) . " " . 
      $order_clause . " " . $limit_clause . ";";

  #$result = mysql_query($query);
  $result = mysqli_query($db,$query);

  #if(!$result || mysql_num_rows($result) == 0) {
  if(!$result || mysqli_num_rows($result) == 0) {
  ?>
  no publications
  <?php
	return;
  }

  # Silly heuristic to decide if dividing by years makes sense
  #if (mysql_num_rows($result) <= 7) {
  if (mysqli_num_rows($result) <= 7) {
      $year_breaks = 0;
      echo "<div class=\"paper-group\">\n";
  } else {
      $year_breaks = 1;
  }

  $previous_year = -1;
  #while($paper = mysql_fetch_assoc($result)) {
  while($paper = mysqli_fetch_assoc($result)) {
      if ($previous_year != $paper["Year"] && $year_breaks) {
          if ($previous_year != -1) {
              echo "</div>";
          }
          echo "<div class=\"yearbreak\">$paper[Year]</div>\n";
          echo "<div class=\"paper-group\">\n";
          $previous_year = $paper["Year"];
      }
  ?>
  <div class="publication">
  <a href="paper/<?php echo tag($db,"Paper",$paper["uid"]); ?>" class="title">
  <?php echo $paper["Title"];?></a><br>
  <?php
  $list = $paper["AuthorList"];
  $authors = explode(",", $list);
  showAuthors($db,$authors);
  ?>
  <br>
  <?php
  $venueYear = $paper["Year"];
  $venueMonth = $paper["Month"];
  $venueVolume = $paper["VolumeNum"];
  $venueNumber = $paper["Number"];
  $shortName = $paper["Venue_short"];
  $fullName = $paper["Venue_full"];
  $type = $paper["Type"];
  $awardPaper = $paper["AwardPaper"];
  $institution = $paper["Institution"];
  $artType = $paper["ArtType"];

  if ($shortName == "") {
      $shortName = $fullName;
  }

  if ($artType) {
      $TR = $artType;
  } else {
      $TR = "Technical Report";
  }

  if( $type == "Journal" || $type == "Magazine" ){
    echo "In $shortName $venueVolume($venueNumber),  ".getMonthText($venueMonth)." $venueYear";
  } else if ($type == "MSThesis") {
    echo "Masters Thesis, $institution " . $venueYear;
  } else if ($type == "PHDThesis") {
    echo "PhD Dissertation, $institution " . $venueYear;
  } else if ($type == "TR") {
      echo "$TR $venueNumber $venueYear";
  } else {
    echo "In $shortName $venueYear";
  }
  if ($awardPaper) {
  ?>
  (<span id="awardPaper">award paper</span>)
  <?php
  }
  ?>
  <span class="paper-links">[ 
  <?php
  $hasPDF = $paper["Pdf"];
  if ($hasPDF) {
  ?>
  <a href="download?uid=<?php echo $paper["uid"];?>">pdf</a> ::
  <?php
  }
  $hasSlides = $paper["slides"];
  if ($hasSlides) {
  ?>
  <a href="download?uid=<?php echo $paper["uid"];?>&amp;slides=1&amp;type=<?php echo $paper["slides_type"];?>" class="pdf">slides</a> ::
  <?php
  }
  $hasWeb = $paper["Web"];
  if ($hasWeb) {
  ?>
  <a target="_blank" href="<?php echo $paper["Web"];?>">website</a> ::
  <?php
  }
  $hasCode = $paper["Code"];
  if ($hasCode) {
  ?>
  <a target="_blank" href="<?php echo $paper["Code"];?>">code</a> ::
  <?php
  }
  ?>
  <a href="bibtex?uid=<?php echo $paper["uid"];?>" onclick="window.open(this.href, '', 'height=300,width=800');return false;">bibtex</a>
  ] </span>
  </div>
  <?php
  }
  echo "</div>\n";

}

function showPersonProfile($db, $uid, $title = "", $projecthack = FALSE) {
  # WARNING: No whitespace from the output of this function, or we get
  # extra space in the layout (since these are now inline, not floated)
  #$query = "SELECT uid, Full_name, Title, Picture IS NOT NULL as Picture, WhereNow FROM Person WHERE uid='$uid' or Tag='$uid'";
  $query = "SELECT uid, Full_name, Title, Picture IS NOT NULL as Picture, WhereNow FROM Person WHERE uid='".mysqli_real_escape_string($db, $uid)."' or Tag='".mysqli_real_escape_string($db, $uid)."'";
  # XXX: Error handling
  #$result = mysql_query($query);
  $result = mysqli_query($db,$query);
  #$row = mysql_fetch_assoc($result);
  $row = mysqli_fetch_assoc($result);

    $pic = $row['Picture'];
    if($pic) {
	$src = "download?uid=".$row["uid"]."&amp;picture=1";
    } else {
	$src = "pics/nobody.jpg";
    }

    if ($title) {
        echo "<div class=\"title-container\"><p class=\"section-title\">$title</p>\n";
    }

    echo '<div class="person-profile">';

    echo '<a href="profile/'.tag($db,'Person',$row['uid']).'" class="name"><img src="'.$src.'" style="width:100%" alt="' . $row['Full_name'] . '"></a>'."<br>";
    echo '<a href="profile/'.tag($db,'Person',$row['uid']).'" class="name">'.$row['Full_name'].'</a><br>';
    if ($row['Title'] == "alum") {
        $titlestr = $row['WhereNow'];
    } else {
        if ($projecthack && $row['Title'] == "affilfaculty") {
            $titlestr = "Faculty";
        } else {
            $titlestr = getPosition($row['Title']);
        }
    }
    echo '<span class="position">'.$titlestr."</span>";
    echo '</div>';
    if ($title) {
        echo "</div>";
    }
}

function showProjectProfile($db, $uid,$short = FALSE) {
    
    # XXX: Error handling
    #$query = "SELECT uid, Name, Description, Picture IS NOT NULL as HasPic FROM Project WHERE uid='$uid' OR Tag='$uid' ORDER BY uid DESC";
    $query = "SELECT uid, Name, Description, Picture IS NOT NULL as HasPic FROM Project WHERE uid='".mysqli_real_escape_string($db, $uid)."' OR Tag='".mysqli_real_escape_string($db, $uid)."' ORDER BY uid DESC";
    #$result = mysql_query($query);
    $result = mysqli_query($db,$query);
    #$row = mysql_fetch_assoc($result);
    $row = mysqli_fetch_assoc($result);

    echo "<div class=\"project-profile\">\n";
    if ($row["HasPic"]) {
        echo "<a href=\"project/" . tag($db,"Project",$row['uid']) . "\"><img class=\"projlogo-small\" src=\"download?uid=$row[uid]&amp;logo=1\" alt=\"$row[Name]\"></a>\n";
    }
    echo "<a href=\"project/" . tag($db,"Project",$row['uid']) . "\" class=\"name\">$row[Name]</a><br>\n";
    if (!$short) {
        echo "<div class=\"description\">$row[Description]</div>\n";
    }
    echo "<div class=\"clear\"></div>\n";
    echo "</div>\n";
}

function showCourseProfile($db, $uid, $title_only = FALSE, $short = FALSE) {
    
    # XXX: Error handling
    #$query = "SELECT uid, Name, Title, Description, Picture IS NOT NULL as HasPic FROM Course WHERE uid='$uid' OR Tag='$uid' ORDER BY uid DESC";
    $query = "SELECT uid, Name, Title, Description, Picture IS NOT NULL as HasPic FROM Course WHERE uid='".mysqli_real_escape_string($db, $uid)."' OR Tag='".mysqli_real_escape_string($db, $uid)."' ORDER BY uid DESC";
    #$result = mysql_query($query);
    $result = mysqli_query($db,$query);
    #$row = mysql_fetch_assoc($result);
    $row = mysqli_fetch_assoc($result);

    echo "<div class=\"course-profile\">\n";
    if ($title_only) {
        if (empty($row['Name'])) {
            echo "<a href=\"course/" . tag($db,"Course",$row['uid']) . "\" class=\"name\">$row[Title]</a><br>\n";
        } else {
            echo "<a href=\"course/" . tag($db,"Course",$row['uid']) . "\" class=\"name\">$row[Name]</a><br>\n";
        }
    } else {
        if ($row["HasPic"]) {
            if (empty($row['Name'])) {
                echo "<a href=\"course/" . tag($db,"Course",$row['uid']) . "\"><img class=\"courselogo-small\" src=\"download?uid=$row[uid]&amp;logo=1\" alt=\"$row[Title]\"></a>\n";
            } else {
                echo "<a href=\"course/" . tag($db,"Course",$row['uid']) . "\"><img class=\"courselogo-small\" src=\"download?uid=$row[uid]&amp;logo=1\" alt=\"$row[Name]\"></a>\n";
            }
        }
        if ($short) {
            if (empty($row['Name'])) {
                echo "<a href=\"course/" . tag($db,"Course",$row['uid']) . "\" class=\"name-small\">$row[Title]</a><br>\n";
            } else {
                echo "<a href=\"course/" . tag($db,"Course",$row['uid']) . "\" class=\"name-small\">$row[Name]: $row[Title]</a><br>\n";
            }
        } else {
            if (empty($row['Name'])) {
                echo "<a href=\"course/" . tag($db,"Course",$row['uid']) . "\" class=\"name\">$row[Title]</a><br>\n";
            } else {
                echo "<a href=\"course/" . tag($db,"Course",$row['uid']) . "\" class=\"name\">$row[Name]: $row[Title]</a><br>\n";
            }
            echo "<div class=\"description\">$row[Description]</div>\n";
        }
    }
    echo "<div class=\"clear\"></div>\n";
    echo "</div>\n";
}

function showSponsorProfile($db, $uid) {
  # WARNING: No whitespace from the output of this function, or we get
  # extra space in the layout (since these are now inline, not floated)
  #$query = "SELECT uid, Name, WWW, Picture IS NOT NULL as Picture FROM Sponsor WHERE uid='$uid' or Tag='$uid'";
  $query = "SELECT uid, Name, WWW, Picture IS NOT NULL as Picture FROM Sponsor WHERE uid='".mysqli_real_escape_string($db, $uid)."' or Tag='".mysqli_real_escape_string($db, $uid)."'";
  # XXX: Error handling
  #$result = mysql_query($query);
  $result = mysqli_query($db,$query);
  #$row = mysql_fetch_assoc($result);
  $row = mysqli_fetch_assoc($result);

    $pic = $row['Picture'];
    if($pic) {
	$src = "download?uid=".$row["uid"]."&amp;sponsor=1";
    } else {
	$src = "pics/nobody.jpg";
    }

    echo '<div class="sponsor-profile">';
    echo '<a target="_blank" href="'.$row['WWW'].'" class="name"><img src="'.$src.'" style="width:100%" alt="' . $row['Name'] . '"></a>'."<br>";
    echo '<a target="_blank" href="'.$row['WWW'].'" class="name">'.$row['Name'].'</a><br>';
    echo '</div>';
}

function getCoursesModule($db, $tags = array()) {

  if(count($tags) == 0){
  ?>
  no course
  <?php
    return;
  }

  $select_clause = "SELECT uid, Name, Title FROM Course";

  $where_clause = "WHERE ";
  foreach($tags as $tag){
    $where_clause = $where_clause . "Tag='" .mysqli_real_escape_string($db, $tag)."' OR ";
  }
  $where_clause = substr($where_clause, 0, -4);

  $order_clause = "ORDER BY ";
  foreach($tags as $tag){
    $order_clause = $order_clause . "(Tag='" .mysqli_real_escape_string($db, $tag)."') DESC, ";
  }
  $order_clause = substr($order_clause, 0, -7);

  $query = $select_clause . " " . $where_clause . " " . $order_clause . ";";

  #$result = mysql_query($query);
  $result = mysqli_query($db,$query);

  #if(!$result || mysql_num_rows($result) == 0) {
  if(!$result || mysqli_num_rows($result) == 0) {
  ?>
  no course
  <?php
	return;
  }

  if($result && mysqli_num_rows($result) > 0) {
    echo "<div class=\"course-profile\">\n";
    while($course = mysqli_fetch_assoc($result)) {
      $uid = $course["uid"];
      $name = $course["Name"];
      $title = $course["Title"];
      if (empty($name)) {
          echo "<a href=\"course/" . tag($db,"Course",$uid) . "\" class=\"name-small\">$title</a><br>\n";
      } else {
          echo "<a href=\"course/" . tag($db,"Course",$uid) . "\" class=\"name-small\">$name: $title</a><br>\n";
      }
    }
    echo "</div>\n";
  }
}

function checkPaperExists($db, $uid) {
    #$result = mysql_query("SELECT * from Paper where uid='$uid';");
    $result = mysqli_query($db,"SELECT * from Paper where uid='$uid';");
    #return mysql_num_rows($result) == 1;
    return mysqli_num_rows($result) == 1;
}

function showAuthor($db, $id) {
    $obj = array();
    if (is_numeric($id)) {
        # Look 'em up in the database
	#$author = mysql_fetch_assoc(mysql_query("SELECT uid,Full_name,Title,WWW FROM Person WHERE uid=$id"));
	#$author = mysqli_fetch_assoc(mysqli_query($db,"SELECT uid,Full_name,Title,WWW FROM Person WHERE uid=$id"));
	$author = mysqli_fetch_assoc(mysqli_query($db,"SELECT uid,Full_name,Title,WWW FROM Person WHERE uid=".mysqli_real_escape_string($db, $id)));
	if($author["Title"] == "other") {
            if($author["WWW"] != null && $author["WWW"] != "") {
                $rv = "<a href=\"".$author["WWW"]."\" class=\"author\">".$author["Full_name"]."</a>";
            } else {
                $rv = "<span>".$author["Full_name"]."</span>";
            }
        } else {
            $rv = "<a href=\"profile/".tag($db,"Person",$author["uid"])."\" class=\"author\">".$author["Full_name"]."</a>";
        }
    } else {
        # Not in the DB, just a name
        $rv = "$id";
    }

    return $rv;
}

function showAuthors($db, $authors) {
    echo "<span class=\"author-list\">";
    for($i=0; $i<count($authors); $i++) {
        echo showAuthor($db, $authors[$i]);
        if($i == count($authors)-1) {
            echo " \n";
        } else if($i == count($authors)-2) {
            if (count($authors) > 2) {
                echo ", and \n";
            }
            else {
                echo " and \n";
            }
        } else {
            echo ", \n";
        }
    }
    echo "</span>";

}

function showVenueForPaper($db, $uid) {
    #$venueInfo = mysql_fetch_assoc(mysql_query("SELECT Venue_full, Venue_short, Year, Month, VolumeNum, Number, Type, Institution, ArtType FROM Paper WHERE uid=$uid"));
    #$venueInfo = mysqli_fetch_assoc(mysqli_query($db,"SELECT Venue_full, Venue_short, Year, Month, VolumeNum, Number, Type, Institution, ArtType FROM Paper WHERE uid=$uid"));
    $venueInfo = mysqli_fetch_assoc(mysqli_query($db,"SELECT Venue_full, Venue_short, Year, Month, VolumeNum, Number, Type, Institution, ArtType FROM Paper WHERE uid=".mysqli_real_escape_string($db, $uid)));
    $type = $venueInfo['Type'];
    if($type == "Journal" || $type == "Magazine" ){
        echo $venueInfo['Venue_full'];
        if ($venueInfo["Venue_short"]) {
            " (".$venueInfo['Venue_short'].")";
        }
        echo " " . $venueInfo['VolumeNum']."(".$venueInfo['Number']."),  ".getMonthText($venueInfo['Month'])." ".$venueInfo['Year'];
    } else if ($type == "MSThesis") {
        echo "Masters Thesis, $venueInfo[Institution]. " .getMonthText($venueInfo['Month'])." ".$venueInfo['Year'];
    } else if ($type == "PHDThesis") {
        echo "PhD Dissertation, $venueInfo[Institution]. " .getMonthText($venueInfo['Month'])." ".$venueInfo['Year'];
    } else if ($type == "TR") {
        if (isset($venueInfo["ArtType"]) && $venueInfo["ArtType"]) {
            echo "$venueInfo[ArtType] ";
        } else {
            echo "Technical Report ";
        }
        echo "$venueInfo[Number], $venueInfo[Institution]. $venueInfo[Year]";
    } else {
        echo $venueInfo["Venue_full"];
        if ($venueInfo["Venue_short"]) {
                echo " (".$venueInfo["Venue_short"].")";
        }
        echo " " . $venueInfo["Year"];
    }
    echo ".";
}

function peopleList($db, $options) {
    $select_clause = "SELECT uid, Full_name, Title, Picture IS NOT NULL AS Picture FROM Person as P ";
    $where_clauses = array("1");
    $order_clause = "ORDER BY Title, SUBSTRING_INDEX(Full_name,' ',-1)";

    if (isset($options['areaID'])) {
        $select_clause .= "LEFT JOIN RelPersonArea as RA on P.uid = RA.PersonID ";
        #array_push($where_clauses,"RA.AreaID=$options[areaID]");
        array_push($where_clauses,"RA.AreaID=".mysqli_real_escape_string($db, $options['areaID']));
    }

    if (isset($options['projectID'])) {
        $select_clause .= "LEFT JOIN RelProjectPerson as RB on P.uid = RB.PersonID ";
        #array_push($where_clauses,"RB.ProjectID=$options[projectID]");
        array_push($where_clauses,"RB.ProjectID=".mysqli_real_escape_string($db, $options['projectID']));
    }

    if (isset($options['courseID'])) {
        $select_clause .= "LEFT JOIN RelCoursePerson as RB on P.uid = RB.PersonID ";
        #array_push($where_clauses,"RB.CourseID=$options[courseID]");
        array_push($where_clauses,"RB.CourseID=".mysqli_real_escape_string($db, $options['courseID']));
    }

    if (!isset($options['includeOther'])) {
        array_push($where_clauses,"P.Title != 'other'");
    }

    if (!isset($options['includeAlumni']) && !isset($options['alumniOnly'])) {
        array_push($where_clauses,"P.Title != 'alum'");
    }

    if (!isset($options['includeAffil'])) {
        array_push($where_clauses,"P.Title != 'affilfaculty'");
    }

    if (isset($options['alumniOnly'])) {
        array_push($where_clauses,"P.Title = 'alum'");
    }


    $query = $select_clause . " WHERE " . join(" AND ", $where_clauses) . " " . 
        $order_clause . ";";

    
    #$result = mysql_query($query);
    $result = mysqli_query($db,$query);

    #if(!$result || mysql_num_rows($result) == 0) {
    if(!$result || mysqli_num_rows($result) == 0) {
        echo "no people";
        return;
    }

    $shownTitle = FALSE;
    #while($person = mysql_fetch_assoc($result)) {
    while($person = mysqli_fetch_assoc($result)) {
        $title = "";
        if (!empty($options['firstHeader']) && !$shownTitle) {
            $title = $options['firstHeader'];
            $shownTitle = TRUE;
        }
        showPersonProfile($db, $person["uid"], $title, isset($options['projectHack']));
    }

}

function hasThesis($db, $uid) {
    #$result = mysql_query("SELECT 1 FROM RelPaperAuthor AS R " .
    #$result = mysqli_query($db,"SELECT 1 FROM RelPaperAuthor AS R " .
    #    " LEFT JOIN Paper AS P on R.PaperID = P.uid WHERE R.AuthorID = $uid " .
    #    " AND (P.Type=\"MSThesis\" OR P.Type=\"PHDThesis\");");
    $result = mysqli_query($db,"SELECT 1 FROM RelPaperAuthor AS R " .
        " LEFT JOIN Paper AS P on R.PaperID = P.uid WHERE R.AuthorID = ".mysqli_real_escape_string($db, $uid) .
        " AND (P.Type=\"MSThesis\" OR P.Type=\"PHDThesis\");");
    #return (mysql_num_rows($result) > 0);
    return (mysqli_num_rows($result) > 0);

}

function showMSThesis($db, $uid) {
    showThesis($db, "MS", $uid);
}

function showPhDThesis($db, $uid) {
    showThesis($db, "PHD", $uid);
}

function showThesis($db, $type, $uid) {
    #$thesis = mysql_query("SELECT P.Title, P.uid FROM RelPaperAuthor AS R " .
    #$thesis = mysqli_query($db,"SELECT P.Title, P.uid FROM RelPaperAuthor AS R " .
    #    " LEFT JOIN Paper AS P on R.PaperID = P.uid WHERE R.AuthorID = $uid " .
    #    " AND P.Type=\"${type}Thesis\";");
    $thesis = mysqli_query($db,"SELECT P.Title, P.uid FROM RelPaperAuthor AS R " .
        " LEFT JOIN Paper AS P on R.PaperID = P.uid WHERE R.AuthorID = ".mysqli_real_escape_string($db, $uid) .
        " AND P.Type=\"${type}Thesis\";");
    #if (mysql_num_rows($thesis)) {
    if (mysqli_num_rows($thesis)) {
        # We assume they've only done one! :)
        #$row = mysql_fetch_assoc($thesis);
        $row = mysqli_fetch_assoc($thesis);
        echo "<tr><td>";
        echo "<span class=\"small-header\">";
        if ($type == "MS") {
            echo "ms thesis";
        } else {
            echo "phd dissertation";
        }
        echo "</span></td> ";
        echo "<td><a href=\"paper/" . tag($db,"Paper",$row['uid']) . "\">$row[Title]</a>";
        echo "</td></tr>";
    }
}

function tag($db, $type, $uid) {
    #$result = mysql_query("SELECT Tag FROM $type WHERE UID=$uid");
    #$result = mysqli_query($db,"SELECT Tag FROM $type WHERE UID=$uid");
    $result = mysqli_query($db,"SELECT Tag FROM $type WHERE UID=".mysqli_real_escape_string($db, $uid));
    #$row = mysql_fetch_assoc($result);
    $row = mysqli_fetch_assoc($result);
    if (isset($row['Tag'])) {
        return $row['Tag'];
    } else {
        return $uid;
    }
}

function lookupByTag($db, $type,$tag) {
    #$result = mysql_query("SELECT uid FROM $type WHERE Tag='$tag'");
    #$result = mysqli_query($db,"SELECT uid FROM $type WHERE Tag='$tag'");
    $result = mysqli_query($db,"SELECT uid FROM $type WHERE Tag='".mysqli_real_escape_string($db, $tag)."'");
    #if (mysql_num_rows($result)) {
    if (mysqli_num_rows($result)) {
        #$row = mysql_fetch_assoc($result);
        $row = mysqli_fetch_assoc($result);
        return $row['uid'];
    } else {
        return FALSE;
    }
    #$row = mysql_fetch_assoc($result);
    $row = mysqli_fetch_assoc($result);
}

function getIDFromParams($db, $type, $nonfatal = TRUE) {
    $id = filter_input(INPUT_GET, "uid", FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[\w-]+$/")));
    if($id == NULL && $id == FALSE) {
        if ($nonfatal){
            return NULL;
        } else {
            die("Invalid unique id");
        }
    }
    if (is_numeric($id)) {
        return $id;
    } else {
        $tag = lookupByTag($db,$type,$id);
        if ($tag) {
            return $tag;
        } else {
            if ($nonfatal){
                return NULL;
            } else {
                die("Invalid unique id");
            }
        }
    }
}

function personHasProjects($db, $uid) {
    #return (mysql_num_rows(mysql_query("SELECT 1 FROM RelProjectPerson WHERE PersonID='$uid' ")) > 0);
    #return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelProjectPerson WHERE PersonID='$uid' ")) > 0);
    return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelProjectPerson WHERE PersonID='".mysqli_real_escape_string($db, $uid)."' ")) > 0);
}

function personHasCourses($db, $uid) {
    #return (mysql_num_rows(mysql_query("SELECT 1 FROM RelCoursePerson WHERE PersonID='$uid' ")) > 0);
    #return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelCoursePerson WHERE PersonID='$uid' ")) > 0);
    return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelCoursePerson WHERE PersonID='".mysqli_real_escape_string($db, $uid)."' ")) > 0);
}

function personHasPapers($db, $uid) {
    #return (mysql_num_rows(mysql_query("SELECT 1 FROM RelPaperAuthor WHERE AuthorID='$uid' ")) > 0);
    #return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelPaperAuthor WHERE AuthorID='$uid' ")) > 0);
    return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelPaperAuthor WHERE AuthorID='".mysqli_real_escape_string($db, $uid)."' ")) > 0);
}

function projectHasPapers($db, $uid) {
    #return (mysql_num_rows(mysql_query("SELECT 1 FROM RelProjectPaper WHERE ProjectID='$uid' ")) > 0);
    #return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelProjectPaper WHERE ProjectID='$uid' ")) > 0);
    return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelProjectPaper WHERE ProjectID='".mysqli_real_escape_string($db, $uid)."' ")) > 0);
}

function projectHasAlumni($db, $uid) {
    #return (mysql_num_rows(mysql_query("SELECT 1 FROM RelProjectPerson AS R LEFT JOIN Person AS P ON R.PersonID=P.uid WHERE ProjectID='$uid' AND P.Title='alum'")) > 0);
    #return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelProjectPerson AS R LEFT JOIN Person AS P ON R.PersonID=P.uid WHERE ProjectID='$uid' AND P.Title='alum'")) > 0);
    return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelProjectPerson AS R LEFT JOIN Person AS P ON R.PersonID=P.uid WHERE ProjectID='".mysqli_real_escape_string($db, $uid)."' AND P.Title='alum'")) > 0);
}

function projectHasSponsors($db, $uid) {
    #return (mysql_num_rows(mysql_query("SELECT 1 FROM RelProjectSponsor WHERE ProjectID='$uid' ")) > 0);
    #return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelProjectSponsor WHERE ProjectID='$uid' ")) > 0);
    return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelProjectSponsor WHERE ProjectID='".mysqli_real_escape_string($db, $uid)."' ")) > 0);
}

function courseHasAlumni($db, $uid) {
    #return (mysql_num_rows(mysql_query("SELECT 1 FROM RelCoursePerson AS R LEFT JOIN Person AS P ON R.PersonID=P.uid WHERE CourseID='$uid' AND P.Title='alum'")) > 0);
    #return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelCoursePerson AS R LEFT JOIN Person AS P ON R.PersonID=P.uid WHERE CourseID='$uid' AND P.Title='alum'")) > 0);
    return (mysqli_num_rows(mysqli_query($db,"SELECT 1 FROM RelCoursePerson AS R LEFT JOIN Person AS P ON R.PersonID=P.uid WHERE CourseID='".mysqli_real_escape_string($db, $uid)."' AND P.Title='alum'")) > 0);
}
