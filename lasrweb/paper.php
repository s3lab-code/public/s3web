<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
require "header.inc";
require "tools.inc";
error_reporting(E_ALL);
ini_set('display_errors', 1);

$id = getIDFromParams($db,"Paper");

if (!checkPaperExists($db,$id)) {
    die("Invalid unique id");
}

#$result = mysql_query("SELECT uid, Title, AuthorList, Abstract, Venue_full, Venue_short, PageNum, DOI, Copyright, AwardPaper, Pdf IS NOT NULL as Pdf, slides IS NOT NULL as slides, slides_type, Web, Code from Paper where uid=$id;");
$result = mysqli_query($db,"SELECT uid, Title, AuthorList, Abstract, Venue_full, Venue_short, PageNum, DOI, Copyright, AwardPaper, Pdf IS NOT NULL as Pdf, slides IS NOT NULL as slides, slides_type, Web, Code from Paper where uid=$id;");
#$paper = mysql_fetch_assoc($result);
$paper = mysqli_fetch_assoc($result);
$title = $paper["Title"];
$authors = getAuthorIDs($paper["AuthorList"]);
$numAuthors = count($authors);
$abstract = $paper["Abstract"];
$doi = $paper["DOI"];
$copyright = $paper["Copyright"];
$pages = explode("-",$paper["PageNum"]);
$page1 = $pages[0];
if(count($pages) >1) {
        $page2 = $pages[1];
} else {
        $page2 = "";
}
$awardPaper = $paper["AwardPaper"];

SPITHEADER($title);

?>

<body>
<div id="paper-page" class="page-container">

<?php require "title.inc"; ?>

<?php SPITMENU("publications"); ?>

<div id="profile-box" class="box-shadow papers-listing">

<div class="paper-group">

<div class="publication">

<p class="section-title">
<?php echo $title; ?>
</p>

<div id="preview">
<?php
  if ($paper["Pdf"]) {
?>
<a href="download?uid=<?php echo $paper["uid"];?>"><img src="download?uid=<?php echo $paper["uid"];?>&amp;preview=1" style="width:100%" class="box-shadow"></a>
<?php
  }
  else {
?>
    <img src="pics/no-pdf.jpg" style="width:100%" class="box-shadow" alt="No PDF availalbe">
<?php
  }
?>
</div>

<div id="metadata">
<p>
<?php showAuthors($db,$authors); ?>
</p>

<p id="venue-list">
<?php showVenueForPaper($db,$id); ?>
</p>

<?php if (isset($doi) && $doi) {
    echo "<p id=\"doi\">DOI: <a target=\"_blank\" href=\"https://dx.doi.org/$doi\">$doi</a></p>\n";
} ?>

<?php if (isset($copyright) && $copyright) {
    echo "<p id=\"copyright\">$copyright</p>\n";
} ?>


<?php 
if ($awardPaper) {
?>
<p class="award-paper">Award paper.</p>
<?php
}
?>
<p id="paper-links">
View 
<?php
  $hasPDF = ($paper["Pdf"]);
  $hasSlides = ($paper["slides"]);
  $hasWeb = ($paper["Web"]);
  $hasCode = ($paper["Code"]);

  $viewCount = 1;
  if ($hasPDF) { $viewCount++; }
  if ($hasSlides) { $viewCount++; }
  if ($hasWeb) { $viewCount++; }
  if ($hasCode) { $viewCount++; }
  $totalCount = $viewCount;

  if ($hasPDF) {
?>
<a href="download?uid=<?php echo $paper["uid"];?>" class="pdf">PDF</a><?php
    $viewCount--;
    if ($viewCount > 0) {
      if ($totalCount == 2) { echo " or "; }
      else {
        echo ", ";
        if ($viewCount == 1) { echo "or "; }
      }
    }
  }
  if ($hasSlides) {
?>
<a href="download?uid=<?php echo $paper["uid"];?>&amp;slides=1&amp;type=<?php echo $paper["slides_type"];?>" class="pdf">slides</a><?php
    $viewCount--;
    if ($viewCount > 0) {
      if ($totalCount == 2) { echo " or "; }
      else {
        echo ", ";
        if ($viewCount == 1) { echo "or "; }
      }
    }
  }
  if ($hasWeb) {
?>
<a target="_blank" href="<?php echo $paper["Web"];?>" class="pdf">website</a><?php
    $viewCount--;
    if ($viewCount > 0) {
      if ($totalCount == 2) { echo " or "; }
      else {
        echo ", ";
        if ($viewCount == 1) { echo "or "; }
      }
    }
  }
  if ($hasCode) {
?>
<a target="_blank" href="<?php echo $paper["Code"];?>">code</a><?php
    $viewCount--;
    if ($viewCount > 0) {
      if ($totalCount == 2) { echo " or "; }
      else {
        echo ", ";
        if ($viewCount == 1) { echo "or "; }
      }
    }
  }
?>
<a href="bibtex?uid=<?php echo $paper["uid"];?>" onclick="window.open(this.href, '', 'height=300,width=800');return false;" class="bibtex">BibTeX</a>.
</p>

<p id="area-list">
<?php
#$result = mysql_query("SELECT A.Name,A.uid FROM ResearchArea as A, RelPaperArea as R, Paper as P WHERE A.uid = R.AID AND P.uid=R.PaperID AND P.uid=$id");
#$result = mysqli_query($db,"SELECT A.Name,A.uid FROM ResearchArea as A, RelPaperArea as R, Paper as P WHERE A.uid = R.AID AND P.uid=R.PaperID AND P.uid=$id");
$result = mysqli_query($db,"SELECT A.Name,A.uid FROM ResearchArea as A, RelPaperArea as R, Paper as P WHERE A.uid = R.AID AND P.uid=R.PaperID AND P.uid=".mysqli_real_escape_string($db, $id));
#//echo mysql_num_rows($result);
//echo mysqli_num_rows($result);
$i = 0;
#if(mysql_num_rows($result) > 0) {
if(mysqli_num_rows($result) > 0) {
?>
<span class="small-header">areas</span><br>
<?php
}
#while($area = mysql_fetch_assoc($result)) {
while($area = mysqli_fetch_assoc($result)) {
	if($i > 0) {
		echo ", \n";
	}
	echo "<a href=\"pubs/".tag($db,"ResearchArea",$area["uid"])."\" class=\"area\">".$area["Name"]."</a>";
	$i++;
}
?>
</p>


<p id="abstract">
<?php if($abstract != "") { ?>
	<span class="small-header">abstract</span><br>
	<?php echo $abstract;
} ?> 
</p>

<div style="clear:both;"></div>

</div>
</div>
</div>
</div>

<?php
#$result = mysql_query("SELECT ProjectID from RelProjectPaper WHERE PaperID='$id'");
#$result = mysqli_query($db,"SELECT ProjectID from RelProjectPaper WHERE PaperID='$id' ORDER BY ProjectID DESC");
$result = mysqli_query($db,"SELECT ProjectID from RelProjectPaper WHERE PaperID='".mysqli_real_escape_string($db, $id)."' ORDER BY ProjectID DESC");
#if (mysql_num_rows($result) > 0) {
if (mysqli_num_rows($result) > 0) {
?>

<div class="box-shadow projects-listing">
<p class="section-title">related project<?php if (mysqli_num_rows($result) > 1) { echo "s"; } ?></p>
<div id="projects" class="masonry-container">
<?php
    #while($row = mysql_fetch_assoc($result)) {
    while($row = mysqli_fetch_assoc($result)) {
        showProjectProfile($db,$row['ProjectID']);
    }
?>
</div>
</div>
<div style="clear:both;"> </div>

<?php
}
?>

</div>

</body>
</html>


