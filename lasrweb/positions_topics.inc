<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<ul>
<li>
  <b>Security of Autonomous &amp; Robotic Vehicles</b> &ndash;
  <a href="project/autoinsight">AutoInsight</a>,
  <a href="project/retrov">RetroV</a>
</li>
<li>
  <b>Trusted Embedded and IoT Devices</b> &ndash;
  <a href="project/trusted-things">Trusted Things</a>
  <!--<a href="project/retrofirm">RetroFirm</a>-->
</li>
<li>
  <b>Peripheral Device Security</b> &ndash;
  <a href="project/perishield">PeriShield</a>
</li>
<li>
  <b>Confidential AI Computing in Cloud &amp; Edge Platforms</b> &ndash;
  <a href="project/aivault">AI Vault</a>
</li>
<li>
  <b>Operating System Security</b> &ndash;
  <a href="project/clue">CLUE</a>,
  <a href="project/shear">Shear</a>
</li>
</ul>
