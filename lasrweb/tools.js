
function addAuthor() {

  var numAuthors = document.getElementById('num').value;
  //authors = authors + "k";  
  //alert("value = "+authors);
  if(numAuthors >= 30) {
	return;
  }
  key = "spanauthor"+numAuthors;
  //window.alert("showing "+key);
 
  if(window.Effect) {
	new Effect.Appear($(key), {duration: 0.6});
  } else { 
  	document.getElementById(key).style.display="block";
  }
  //document.getElementById(key).style.visibility="visible";
  //document.getElementById(key).style.position="static";	
  numAuthors++;
  document.getElementById("num").value = numAuthors;
  
}

function lessAuthors() {
  var numAuthors = document.getElementById('num').value;
  if(numAuthors <= 1) {
        return;
  }
  numAuthors--;
  document.getElementById("num").value = numAuthors;
  key = "spanauthor"+numAuthors;
  //window.alert("hiding "+key);
  if(window.Effect) {
	new Effect.Fade($(key), {duration: 0.3});
  } else {
  	document.getElementById(key).style.display="none";
  }
  //document.getElementById(key).style.visibility="hidden";
  //document.getElementById(key).style.position="absolute";
}
