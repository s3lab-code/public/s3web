<?php
#
# Parameters
#

# Database info
$DATABASE_HOST = "database";
$DATABASE_NAME = "s3web";
$DATABASE_USERNAME = "s3web";
$DATABASE_PASSWORD = "ZK4epHJ2cjDf5kVw";

# This title will be given to every page
$PAGETITLE = "S3 Lab - Software & Systems Security Laboratory";

# Base URL. Note: Don't inlcude http://, that gets added automatically
$BASE = "www.s3lab.io/";

# Extra stuff to stick in the headers - eg. Google Analytics data
$EXTRA_HEADERS = <<<XML
<!-- Default Statcounter code for S3 Lab
https://www.s3lab.io/ -->
<script type="text/javascript">
var sc_project=12745324; 
var sc_invisible=1; 
var sc_security="70b1c0d4"; 
</script>
<script type="text/javascript"
src="https://www.statcounter.com/counter/counter.js"
async></script>
<noscript><div class="statcounter"><a title="Web Analytics"
href="https://statcounter.com/" target="_blank"><img
class="statcounter"
src="https://c.statcounter.com/12745324/0/70b1c0d4/1/"
alt="Web Analytics"
referrerPolicy="no-referrer-when-downgrade"></a></div></noscript>
<!-- End of Statcounter Code -->
XML;

?>
