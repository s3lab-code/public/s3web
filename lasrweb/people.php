<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
require "header.inc";
require "tools.inc";

SPITHEADER("People");
?>

<body>
<div id="people-page" class="page-container">

<?php require "title.inc"; ?>

<?php SPITMENU("people") ?>

<div class="box-shadow people-listing">

<p class="section-title">
current group members
</p>

<div id="filter-options">
Showing all current members.
View people working on 
<?php
$query = "select DISTINCT AreaID, Name from RelPersonArea join ResearchArea on AreaID=uid;";

#$result = mysql_query($query);
$result = mysqli_query($db,$query);
#$num_areas = mysql_num_rows($result);
$num_areas = mysqli_num_rows($result);
$cnt = 1;
#while($row = mysql_fetch_assoc($result)){
while($row = mysqli_fetch_assoc($result)){
    if($cnt < $num_areas){
      echo '<a href="area/'.tag($db,"ResearchArea",$row['AreaID']).'">'.strtolower($row['Name']).'</a>,'."\n";
    } else {
      echo 'or'."\n";
      echo '<a href="area/'.tag($db,"ResearchArea",$row['AreaID']).'">'.strtolower($row['Name']).'</a>.'."\n";
    }
    $cnt++;
}
?>

<!--Or, view <a href="people#alum">former members</a>, or <a href="people#collab">collaborators</a>.-->
</div>
<div style="clear:both;">

<?php 
  
$positions_to_show = array("faculty", "scientist","staff","postdoc", "phd", "masters", "undergrad", "visitor", "other");

foreach ($positions_to_show as $poskey)
{
  #$query = "SELECT uid FROM Person "
  #      ."WHERE Title='".$poskey."' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1);";
  $query = "SELECT uid FROM Person "
        ."WHERE Title='".mysqli_real_escape_string($db, $poskey)."' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1);";

  #$result = mysql_query($query);
  $result = mysqli_query($db,$query);

  #while($row = mysql_fetch_assoc($result)){
  while($row = mysqli_fetch_assoc($result)){
    
      showPersonProfile($db,$row['uid']);
  }
}

?>

<div style="clear:both;"> </div>

</div>
</div>

<?php
$query = "SELECT uid FROM Person "
        ."WHERE Title='alum' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1);";

#$result = mysql_query($query);
$result = mysqli_query($db,$query);
$num_people = mysqli_num_rows($result);
if($num_people > 0) {
?>

<a id="alum">
</a>

<div class="box-shadow people-listing">

<p class="section-title">
former members
</p>

<?php
#while($row = mysql_fetch_assoc($result)){
while($row = mysqli_fetch_assoc($result)){
    
    showPersonProfile($db,$row['uid']);
}
?>

<div style="clear:both;"> </div>
</div>

<?php
}
?>

<?php
$query = "SELECT uid FROM Person "
        ."WHERE Title='ext' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1);";

#$result = mysql_query($query);
$result = mysqli_query($db,$query);
$num_people = mysqli_num_rows($result);
if($num_people > 0) {
?>

<a id="collab">
</a>

<div class="box-shadow people-listing">

<p class="section-title">
affilated faculty and external collaborators
</p>

<?php
#while($row = mysql_fetch_assoc($result)){
while($row = mysqli_fetch_assoc($result)){
    
    showPersonProfile($db,$row['uid']);
}
?>

<div style="clear:both;"> </div>
</div>

<?php
}
?>

</div>

</body>
</html>


