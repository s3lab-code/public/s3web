<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
require "header.inc";
require "tools.inc";

$id = getIDFromParams($db,"ResearchArea",TRUE);

if ($id) {
    #$result2 = mysql_query("SELECT Name FROM ResearchArea WHERE uid='$id'");
    #$result2 = mysqli_query($db,"SELECT Name FROM ResearchArea WHERE uid='$id'");
    $result2 = mysqli_query($db,"SELECT Name FROM ResearchArea WHERE uid='".mysqli_real_escape_string($db, $id)."'");
    #$row2 = mysql_fetch_assoc($result2);
    $row2 = mysqli_fetch_assoc($result2);
    $areaname = "$row2[Name]";
} else {
    $areaname = "";
}

if ($id) {
    SPITHEADER("$areaname Publications");
} else {
    SPITHEADER("Publications");
}

?>

<body>
<div id="listing-page" class="page-container">

<?php require "title.inc"; ?>

<?php SPITMENU("publications"); ?>

<div class="box-shadow papers-listing">

<p class="section-title">
<?php if ($id) { echo strtolower($areaname) . " "; } ?>
publications
<?php
    if ($id) {
        echo "<span class=\"subtitle\"><a href=\"area/" . tag($db,"ResearchArea",$id) . "\">(see more about " . strtolower($areaname) . ")</a></span>";
    }
?>
</p>

<div id="filter-options">
<?php if (!$id) { echo "Showing all publications. "; } ?>
View publications on
<?php
$query = "SELECT RA.uid AS AreaID, RA.Name FROM ResearchArea AS RA LEFT JOIN RelPaperArea AS REL on RA.uid = REL.AID GROUP BY RA.uid ORDER BY COUNT(*) DESC, RA.uid ASC";

#$result = mysql_query($query);
$result = mysqli_query($db,$query);
#$num_areas = mysql_num_rows($result);
$num_areas = mysqli_num_rows($result);
$cnt = 0;
#while($row = mysql_fetch_assoc($result)){
while($row = mysqli_fetch_assoc($result)){
    if($cnt == $num_areas - 2){
      echo '<a href="pubs/'.tag($db,"ResearchArea",$row['AreaID']).'">'.strtolower($row['Name']).'</a>'."\n";
    } else if ($cnt == $num_areas - 1) {
      echo 'or'."\n";
      echo '<a href="pubs/'.tag($db,"ResearchArea",$row['AreaID']).'">'.strtolower($row['Name']).'</a>.'."\n";
    } else {
      echo '<a href="pubs/'.tag($db,"ResearchArea",$row['AreaID']).'">'.strtolower($row['Name']).'</a>,'."\n";
    }
    $cnt++;
}
?>
<?php if ($id) { echo "Or, show <a href=\"pubs\">all publications</a>. "; } ?>
</div>
<div style="clear:both;"></div>

<?php
echo getPublicationsModule($db,$id?array("areaID" => $id):array());
?>

</div>
</div>

</body>
</html>


