<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<ul>
<li>
  <b>High Fluency in C/C++ and Python Code</b>
</li>
<li>
  <b>Strong Familiarity with Linux Command Line Environment</b>
</li>
<li>
  <b>Excellent Technical Writing Skills in English</b>
</li>
</ul>
