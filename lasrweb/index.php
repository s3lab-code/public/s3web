<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems eesearch (LASR), Department
ystemf Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
require "header.inc";
require "tools.inc";

SPITHEADER("S3 Lab - Software & Systems Security Laboratory","",TRUE);

?>

<body>
<div id="frontpage" class="page-container">

<?php require "title.inc"; ?>

<div id="main-picture" class="box-shadow">
<img src="pics/ecs.jpg" alt="South Engineering and Computer Science Building">

<div id="front-menu"> 
<ul>
<li><a href="people" class="option">People</a></li>
<li><a href="news" class="option">News</a></li>
<li><a href="pubs" class="option">Publications</a></li>
<li><a href="projects" class="option">Projects</a></li>
<li><a href="courses" class="option">Courses</a></li>
<li><a href="positions" class="option">Positions</a></li>
<li><a href="sponsors" class="option">Sponsors</a></li>
<li><a href="contact" class="option">Contact</a></li>
</ul>
</div>

<div id="about-us-box">
  <!-- TRANSPARENT BACKGROUND -->
  <div id="about-us-box-background" class="opaque-75"></div>
  <div id="about-us-box-content">
  <p class="section-title">
  <span class=light>about us</span>
  </p>

  <p>
  We conduct research and build practical tools to analyze the <a href="area/sec">security</a> of diverse software and develop dependable systems using <a href="area/pa">program analysis</a>, <a href="area/os">operating systems</a> and <a href="area/cps">cyber-physical systems</a> techniques.
  </p>

  <p id="about-us-app"><a href="positions">If you are interested in working with us, please look at this page</a>.
  </p>

  <p>S3 Lab is part of the <a target="_blank" href="https://csi.utdallas.edu/">Cyber Security Research and Education Institute</a> and the <a target="_blank" href="https://cs.utdallas.edu/">Department of Computer Science</a> at the <a target="_blank" href="https://www.utdallas.edu/">University of Texas at Dallas</a>.
  </p>
  </div>
</div>

<div id="news-box">
  <!-- TRANSPARENT BACKGROUND -->
  <div id="news-box-background" class="opaque-75"></div>
  <div id="news-box-content">
  <p class="section-title">
    <span class="light">recent news</span>
    <span class="subtitle"><a href="news">(see more)</a></span>
  </p>

  <?php getNewsModule($db,array("limit" => 4, "excludeDates" => 1)); ?>
  </div>
</div>

<div id="link-box">
<a target="_blank" href="https://gitlab.com/s3lab-code/public" class="option">GitLab</a></li> /
<a target="_blank" href="https://overleaf.s3lab.io" class="option">Overleaf</a></li> /
<a target="_blank" href="https://s3lab-workspace.slack.com" class="option">Slack</a></li> /
<a target="_blank" href="https://www.notion.so/s3lab" class="option">Notion</a></li>
</ul>
</div>

</div> <!-- main-picture -->

<table id="frontpagetable" cellspacing=0 cellpadding=0 border=0>
<tr>

<td id="project-box" class="box-shadow projects-listing" rowspan=5>
<p class="section-title">
highlighted projects
<span class="subtitle"><a href="projects">(see more)</a></span>
</p>

<?php showProjectProfile($db,"autoinsight"); showProjectProfile($db,"retrov"); showProjectProfile($db,"trusted-things"); showProjectProfile($db,"aivault"); showProjectProfile($db,"clue"); ?>

</td>

<td style="width:10px;padding:0px;" rowspan=5>
</td>

<td id="areas-box" class="box-shadow areas-listing">
<p class="section-title">research areas</p>
<div class="transparent-box">
<ul>
<?php
#$result = mysql_query("SELECT RA.uid, RA.Name FROM ResearchArea AS RA LEFT JOIN RelPaperArea AS REL on RA.uid = REL.AID GROUP BY RA.uid ORDER BY COUNT(*) DESC");
$result = mysqli_query($db,"SELECT RA.uid, RA.Name FROM ResearchArea AS RA LEFT JOIN RelPaperArea AS REL on RA.uid = REL.AID GROUP BY RA.uid ORDER BY COUNT(*) DESC, RA.uid ASC");
#while ($row = mysql_fetch_assoc($result)) {
while ($row = mysqli_fetch_assoc($result)) {
    echo "<li><a href=\"area/" . tag($db,"ResearchArea",$row['uid']) . "\">$row[Name]</a></li>\n";
}
?>
</ul>
<div class="clear"></div>
</div>
</td>

<tr>
<td style="height:10px;padding:0px;">
</td>
</tr>


<td id="courses-box" class="box-shadow courses-listing">
<p class="section-title">
courses and seminar
<span class="subtitle"><a href="courses">(see more)</a></span>
</p>

<?php
getCoursesModule($db,array("cs6324", "seminar"));
?>

</td>
</tr>

<tr>
<td style="height:10px;padding:0px;">
</td>
</tr>


<tr>
<td id="publications-box" class="box-shadow papers-listing">
<p class="section-title">
recent publications
<span class="subtitle"><a href="pubs">(see more)</a></span>
</p>

<?php getPublicationsModule($db,array("limit" => 7, "excludeTheses" => 1, "excludeTRs" => 1)); ?>
</td>
</tr>

</table>

<!-- LASR footer -->
<div id="footer">
<span style="float:left">Building image courtesy of the University of Texas at Dallas</span>
This site is built using software originally developed by the <a href="https://www.cs.utexas.edu/lasr/" id="lasr">LASR</a> and <a href="https://www.flux.utah.edu/" id="lasr">Flux Research Group</a>.
</div>

</div>

</body>
</html>

