<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
require "header.inc";
require "tools.inc";

SPITHEADER("Positions");

?>

<body>

<div id="positions-page" class="page-container">

<?php require "title.inc"; ?>

<?php SPITMENU("positions"); ?>

<table cellpadding=0 cellspacing=0 style="clear:both">
<tr>
<td id="positions-box" class="box-shadow">
<p class="section-title">positions available</p>

<div class="transparent-box">
<p>
S3 Lab is currently seeking
<b>two</b>
<em>qualified</em>
<!--students at all levels (undergraduate, Masters, and Ph.D.)-->
Ph.D. students for projects in the following <b id="topics"> research topics</b>:

<?php require "positions_topics.inc"; ?>

The following <b>technical skills</b> are <b id="required">required</b> for our research:

<?php require "positions_required.inc"; ?>

Prior experience in any of the following <b id="optional">optional techniques</b> will be a plus:

<?php require "positions_optional.inc"; ?>
</p>

<p>
Our group publishes research papers at <b>top security conferences</b>
(e.g., USENIX Security, CCS and NDSS).
All projects will be <b>open source</b>.
</p>

<p>
Qualified students will be funded for their <b>tuition</b> and
<b>competitive salaries</b>,
sufficient for living in the area without external support.
</p>

If you have <em>expertise</em> in the skills and techniques,
and are willing to <em>commit</em> to a research and development
project in our group,
<b>please fill out
<a id="underline" target="_blank" href="https://forms.gle/Ycr4ekUhGiNUDfQh8">this application form</a></b>.
</p>

</div>

</td>
</tr>

<tr style="height:10px">
</tr>

<tr>
<td id="school-box" class="box-shadow">
<p class="section-title">research environment</p>

<div class="transparent-box">
<p>
S3 Lab is part of the
<a target="_blank" href="https://cs.utdallas.edu/">
Department of Computer Science</a>
at the
<a target="_blank" href="https://www.utdallas.edu/">
University of Texas at Dallas</a> (UT Dallas).
The department has around 53 strong research faculty engaged in
cutting-edge research.
In the past ten years, UT Dallas CS has achieved
<em>top-notch research contributions</em> in multiple areas
(#33 in Security by <a target="_blank" href="http://csrankings.org/">CSRankings.org</a> in 2021).
S3 Lab is also part of the <a target="_blank" href="https://csi.utdallas.edu/">Cyber Security Research and Education Institute</a> with 15 core faculty working in various areas of cyber security research.
</p>

<p>
UT Dallas is one of the fastest-growing public universities
in the United States,
located in the
<a target="_blank" href="https://en.wikipedia.org/wiki/Dallas%E2%80%93Fort_Worth_metroplex">Dallas-Fort Worth</a>
(DFW) metropolitan area.
The university has an <em>outstanding research environment</em> and
it is at a <em>convenient location</em> for students for a living:

<ul>
<li>
<a target="_blank" href="https://news.utdallas.edu/campus-community/ut-dallas-forbes-best-value-ranking-2019/?WT_mc_id=NewsHomePage">#1 best value</a> public university in Texas and
<a target="_blank" href="https://www.chronicle.com/article/fastest-growing-colleges-2008-18">#2 fastest growing</a> public doctoral university in the US.
</li>
<li>
Most Ph.D. students are fully funded through TA/RA-ships
<em>including</em> tuition, stipend, and travel support.
</li>
<li>
The Dallas area has diverse communities from many different countries
(e.g.,
<a target="_blank" href="https://koreatowncarrollton.com/">Koreatown</a>) and
a large international airport
(<a target="_blank" href="https://en.wikipedia.org/wiki/Dallas/Fort_Worth_International_Airport">DFW Airport</a>)
with direct flights to most major cities in the world.
</li>
</ul>
</p>

<p>
<b>UT Dallas CS Admissions:</b>:
<a target="_blank" href="https://cs.utdallas.edu/admissions">https://cs.utdallas.edu/admissions</a>
</p>

</td>
</tr>

</table>

</div>

</body>
</html>

