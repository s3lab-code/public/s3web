<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
require "header.inc";
require "tools.inc";

SPITHEADER("Contact Us");

?>

<body>

<div id="contact-page" class="page-container">

<?php require "title.inc"; ?>

<?php SPITMENU("contact"); ?>

<table cellpadding=0 cellspacing=0 style="clear:both">
<tr>
<td id="mailing-box" class="box-shadow">
<p class="section-title">mailing address</p>
<div class="transparent-box">
(name of recipient)<br>
University of Texas at Dallas<br>
Erik Jonsson School of Engineering & Computer Science<br>
Department of Computer Science<br>
800 W. Campbell Road, MS&nbsp;EC31<br>
Richardson, TX 75080&ndash;3021
</p>
</div>
</td>

<td style="width:10px;padding:0px"></td>

<td id="directions-box" class="box-shadow" rowspan=3>
<p class="section-title">directions</p>

<div class="transparent-box">
<p>
S3 Lab is located
in <a target="_blank" href="https://locator.utdallas.edu/ECSS_4.214">ECSS 4.214</a>
and <a target="_blank" href="https://locator.utdallas.edu/ECSS_3.220">ECSS 3.220</a>
of the <a target="_blank" href="https://maps.app.goo.gl/TSErR47etyYsxf2JA">Engineering and Computer Science South</a> building
(838 Franklyn Jenifer Dr, Richardson, TX 75080) in the center of the UT Dallas campus.
</p>

<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3346.667968671573!2d-96.74871895362014!3d32.986160763266476!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864c21fff682dc29%3A0x5f0c06ff030083e1!2sErik%20Jonsson%20School%20of%20Engineering%20and%20Computer%20Science%2C%20ECSS!5e0!3m2!1sen!2sus!4v1696971450900!5m2!1sen!2sus"></iframe>

<p>The closest parking garage is <a target="_blank" href="https://maps.app.goo.gl/fceviotGfm3kFiYz8">Parking Structure 4 (PS4)</a>, 2-3 blocks away from the ECSS building. The closest parking (surface) lot is <a target="_blank" href="https://maps.app.goo.gl/tpZCpnUatCSR3w117">Parking Lot J</a>, which is slightly closer than the parking garage. Make sure your <a target="_blank" href="https://utdallas.edu/visitors/visitor-parking/">parking permit</a> includes the parking lot that you will use. If you do not have a parking permit, you can use pay-per-use white-colored Visitor parking spaces at PS4. Use the parking machine near the east-end of the parking garage.
</p>

</div>
</td>
</tr>

<tr style="height:10px">
</tr>

<tr>
<td id="contact-box" class="box-shadow people-listing">

<p class="section-title">group leaders</p>

<?php showPersonProfile($db,"chungkim"); echo "<br>"; showPersonProfile($db, "mkpark"); ?>

</td>
</tr>

</table>

</div>

</body>
</html>

