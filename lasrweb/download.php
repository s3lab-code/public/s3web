<?php

/*<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->
*/
require "header.inc";
require "tools.inc";
error_reporting(E_ALL);
ini_set('display_errors', 1);

$id = filter_input(INPUT_GET, "uid", FILTER_VALIDATE_INT);
if($id == NULL && $id == FALSE) {
        die("Invalid unique id");
}

$error = false;

if(isset($_GET["preview"])) {
	$table = "Paper";
	$field = "Thumbnail";
	$mime = "application/jpg";
	$prefix = "image";
	$suffix = "jpg";
} else if(isset($_GET["picture"])) {
	$table = "Person";
	$field = "Picture";
	$mime = "application/jpg";
	$prefix = "profile_image";
	$suffix = "jpg";
} else if(isset($_GET["slides"])) { 
	$table = "Paper";
	$field = "slides";
	$prefix = "slides";
	if(isset($_GET["type"]) && ($_GET["type"]=="key")) {
		$mime = "application/vnd.apple.keynote";
		$suffix = $_GET["type"];
	} else if(isset($_GET["type"]) && ($_GET["type"]=="ppt")) {
                $mime = "application/vnd.ms-powerpoint";
                $suffix = $_GET["type"];
        } else if(isset($_GET["type"]) && ($_GET["type"]=="pps")) {
                $mime = "application/vnd.ms-powerpoint";
                $suffix = $_GET["type"];
        } else if(isset($_GET["type"]) && ($_GET["type"]=="pptx")) {
                $mime = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                $suffix = $_GET["type"];
        } else {
		$mime = "application/pdf";
		$suffix = "pdf";
	}
} else if (isset($_GET["logo"])) {
	$table = "Project";
	$field = "Picture";
	$mime = "application/png";
	$prefix = "project";
	$suffix = "png";
} else if (isset($_GET["sponsor"])) {
	$table = "Sponsor";
	$field = "Picture";
	$mime = "application/png";
	$prefix = "sponsor";
	$suffix = "png";
} else {
	$table = "Paper";
	$field = "Pdf";
	$mime = "application/pdf";
	$prefix = "paper";
	$suffix = "pdf";
}

#$select_query = "SELECT uid,$field,DATE_FORMAT(LastMod,'%a, %d %b %Y %T') AS LastMod, Tag FROM $table WHERE uid = $id AND $field != '' AND $field IS NOT NULL";
$select_query = "SELECT uid,$field,DATE_FORMAT(LastMod,'%a, %d %b %Y %T') AS LastMod, Tag FROM $table WHERE uid = ".mysqli_real_escape_string($db, $id)." AND ".mysqli_real_escape_string($db, $field)." != '' AND ".mysqli_real_escape_string($db, $field)." IS NOT NULL";
#$result = mysql_query($select_query);
$result = mysqli_query($db,$select_query);

#if($result && mysql_num_rows($result)>0) {
if($result && mysqli_num_rows($result)>0) {
        #
        # First, check If-Modified-Since
        #
	#$paper = mysql_fetch_assoc($result);
	$paper = mysqli_fetch_assoc($result);
        $lastmod = $paper['LastMod'];

        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $lastmod == $_SERVER['HTTP_IF_MODIFIED_SINCE']) {
            header("HTTP/1.1 304 Not Modified"); 
            exit;
        }

	$data = $paper[$field];
	$len = strlen($data);

	if($prefix == "paper" || $prefix == "slides") {
                if ($prefix == "slides") {
                    $prefix = "slides-";
                } else {
                    $prefix = "";
                }
                if (empty($paper['Tag'])) {
                    $fileId = $id;
                } else {
                    $fileId = $paper['Tag'];
                }
	} else {
		$fileId=$id;
	}

	header("Content-Type: $mime");
	header("Content-Length: ". $len);
        header('Cache-Control: max-age=36000, public');
        header("Last-Modified: " . $lastmod);
	header("Content-Disposition: attachment; filename=".$prefix.$fileId.".".$suffix);

	echo $data;
} else {
	//$error = true;
	//$ERROR_MSG = "That PDF does not exist";
	die("This file does not exist");
}
?>
