<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
require "header.inc";
require "tools.inc";

$id = getIDFromParams($db,"Person");

#$result = mysql_query("SELECT * from Person where uid=$id;");
$result = mysqli_query($db,"SELECT * from Person where uid=$id;");
if(!$result || empty($result)) {
	die("Invalid unique id");
} 

#$person = mysql_fetch_assoc($result);
$person = mysqli_fetch_assoc($result);

if(empty($person)) {
	die("Invalid unique id");
}

SPITHEADER("$person[Full_name]");

$advisorID = $person["Advisor"];
$advisorID2 = $person["Advisor2"];
#$advisorres1 = mysql_query("SELECT uid, Full_name FROM Person where uid=$advisorID;");
$advisorres1 = mysqli_query($db,"SELECT uid, Full_name FROM Person where uid=$advisorID;");
#$advisorres2 = mysql_query("SELECT uid, Full_name FROM Person where uid=$advisorID2;");
$advisorres2 = mysqli_query($db,"SELECT uid, Full_name FROM Person where uid=$advisorID2;");
#if($advisorres1 && mysql_num_rows($advisorres1) > 0) {
if($advisorres1 && mysqli_num_rows($advisorres1) > 0) {
	#$advisor1 = mysql_fetch_assoc($advisorres1);
	$advisor1 = mysqli_fetch_assoc($advisorres1);
} else {
	$advisor1 = NULL;
}
#if($advisorres2 && mysql_num_rows($advisorres2) > 0) {
if($advisorres2 && mysqli_num_rows($advisorres2) > 0) {
	#$advisor2 = mysql_fetch_assoc($advisorres2);
	$advisor2 = mysqli_fetch_assoc($advisorres2);
} else {
	$advisor2 = NULL;
}

#
# Find advisees
#
#$advisee_res = mysql_query("SELECT uid, Full_name, Title FROM Person WHERE Advisor=$id or Advisor2=$id");
#$advisee_res = mysqli_query($db,"SELECT uid, Full_name, Title FROM Person WHERE Advisor=$id or Advisor2=$id");
$advisee_res = mysqli_query($db,"SELECT uid, Full_name, Title FROM Person WHERE Advisor=".mysqli_real_escape_string($db, $id)." or Advisor2=".mysqli_real_escape_string($db, $id));

$advisees = array();
$alumni = array();

#while($row = mysql_fetch_assoc($advisee_res)) {
while($row = mysqli_fetch_assoc($advisee_res)) {
    $userstring = "<a href=\"profile/" . tag($db,"Person",$row['uid']) . "\">$row[Full_name]</a>";
    if ($row['Title'] == "alum") {
        array_push($alumni,$userstring);
    } else {
        array_push($advisees,$userstring);
    }
}


if($person["Picture"] == null) {
	$src = "pics/nobody.jpg";
} else {
	$src = "download?uid=".$person["uid"]."&amp;picture=1";
}

#$areas = mysql_query("SELECT Name,A.uid FROM RelPersonArea as R,Person as P,ResearchArea as A,RelPaperArea RP WHERE A.uid=R.AreaID AND P.uid=R.PersonID AND P.uid=$id AND A.uid=RP.AID GROUP BY A.uid ORDER BY COUNT(*) DESC, A.uid ASC");
#$areas = mysqli_query($db,"SELECT Name,A.uid FROM RelPersonArea as R,Person as P,ResearchArea as A,RelPaperArea RP WHERE A.uid=R.AreaID AND P.uid=R.PersonID AND P.uid=$id AND A.uid=RP.AID GROUP BY A.uid ORDER BY COUNT(*) DESC, A.uid ASC");
$areas = mysqli_query($db,"SELECT Name,A.uid FROM RelPersonArea as R,Person as P,ResearchArea as A,RelPaperArea RP WHERE A.uid=R.AreaID AND P.uid=R.PersonID AND P.uid=".mysqli_real_escape_string($db, $id)." AND A.uid=RP.AID GROUP BY A.uid ORDER BY COUNT(*) DESC, A.uid ASC");
?>

<body>
<div id="profile-page" class="page-container">

<?php require "title.inc"; ?>

<?php SPITMENU("people"); ?>

<div id="rightside">

<?php if (personHasCourses($db,$id)) { ?>

<div id="courses-section" class="box-shadow courses-listing">
<p class="section-title">courses &amp; seminar</p>

<?php
#$result = mysql_query("SELECT CourseID FROM RelCoursePerson WHERE PersonID='$id'");
#$result = mysqli_query($db,"SELECT CourseID FROM RelCoursePerson WHERE PersonID='$id' ORDER BY CourseID DESC");
$result = mysqli_query($db,"SELECT CourseID FROM RelCoursePerson WHERE PersonID='".mysqli_real_escape_string($db, $id)."' ORDER BY CourseID DESC");
#while ($row = mysql_fetch_assoc($result)) {
while ($row = mysqli_fetch_assoc($result)) {
    showCourseProfile($db,$row['CourseID'],TRUE,TRUE);
}
?>

</div>

<?php } ?>

<?php if (personHasProjects($db,$id)) { ?>

<div id="projects-section" class="box-shadow projects-listing">
<p class="section-title">projects</p>

<?php
#$result = mysql_query("SELECT ProjectID FROM RelProjectPerson WHERE PersonID='$id'");
#$result = mysqli_query($db,"SELECT ProjectID FROM RelProjectPerson WHERE PersonID='$id' ORDER BY ProjectID DESC");
$result = mysqli_query($db,"SELECT ProjectID FROM RelProjectPerson WHERE PersonID='".mysqli_real_escape_string($db, $id)."' ORDER BY ProjectID DESC");
#while ($row = mysql_fetch_assoc($result)) {
while ($row = mysqli_fetch_assoc($result)) {
    showProjectProfile($db,$row['ProjectID'],TRUE);
}
?>

</div>

<?php } ?>

</div>

<div id="leftside">

<div id="person-section" class="box-shadow people-listing">

<div class="transparent-box">

<div id="picture-cell">
<img src="<?php echo $src; ?>" alt="<?php echo $person["Full_name"]; ?>">
</div>

<div id=metadata>

<p class="section-title">
<?php echo $person["Full_name"]; ?>
</p>

<?php 
if(!is_null($advisor1)) {
  if ($person["Title"] == "postdoc" || $person["Title"] == "visitor" || $person["Title"] == "other") {
?>
<span id="position"><?php echo getPosition($person["Title"]);?></span>
<span id="advisor">with
<?php
  }
  else {
?>
<span id="position"><?php echo getPosition($person["Title"]);?></span>,
<span id="advisor">advised by 
<?php
  }
?>
  <a href="<?php echo "profile/" . tag($db,"Person",$advisor1["uid"]);?>"><?php echo $advisor1["Full_name"];?></a>
  <?php if(!is_null($advisor2)) { ?>  
  and <a href="<?php echo "profile/" . tag($db,"Person",$advisor2["uid"]);?>"><?php echo $advisor2["Full_name"];?></a>
<?php
  }
  ?>
  </span>
<?php
}
else {
?>
  <span id="position"><?php echo getPosition($person["Title"]);?></span>
<?php
}

if ($person["Title"] == "alum" && $person["WhereNow"]) {
    /*echo "<table id=\"aluminfo\" class=\"persondetails\"><tr><td><span class=\"small-header\">now at</span></td><td>$person[WhereNow]</td></tr></table>\n";*/
    echo "<table id=\"aluminfo\" class=\"persondetails\"><tr><td><span class=\"small-header\">first employment</span></td><td>$person[WhereNow]</td></tr></table>\n";
}


# Leave these off faculty pages
if (hasThesis($db,$id) && $person["Title"] != "faculty" && $person["Title"] != "affilfaculty") {
    echo "<table id=\"theses\" class=\"persondetails\">\n";
    showPhDThesis($db,$id);
    showMSThesis($db,$id);
    echo "</table>\n";
}

?>


<?php
#$area_count = mysql_num_rows($areas);
$area_count = mysqli_num_rows($areas);
if ($areas && $area_count != 0) {
  echo "<table id=\"areas\" class=\"persondetails\">\n";
  echo "<tr><td>";
  if ($area_count > 1)
  {
?>
  <span class="small-header">areas of interest</span>
  <?php
  }
  else
  {
?>
  <span class="small-header">area of interest</span>
  <?php
  }
  echo "</td>\n";
  echo "<td>";
  $cnt = 1;
  #while($row = mysql_fetch_assoc($areas)) {
  while($row = mysqli_fetch_assoc($areas)) {
    if($cnt < $area_count) {
      echo "<a href=\"area/".tag($db,"ResearchArea",$row["uid"])."\">".strtolower($row['Name'])."</a>,\n";
    } else {
      echo "<a href=\"area/".tag($db,"ResearchArea",$row["uid"])."\">".strtolower($row['Name'])."</a>\n";
    }
    $cnt++;
  }
  ?>
  </td></tr></table>
<?php
}
?>

<table id="contact" class="persondetails">

<?php if($person["Email"]) { ?>
<tr>
    <td>
        <span class="small-header">email</span>
    </td>
    <td>
        <?php echo str_replace("@"," at ",$person["Email"]);?>
    </td>
</tr>

<?php
} 
if($person["WWW"]) {
  $url = explode("//", $person["WWW"]);
  $url = $url[count($url)-1];
  if ($url[strlen($url)-1] == "/") {
    $url = substr($url, 0, strlen($url)-1);
  }
?>
<tr>
    <td>
        <span class="small-header">web</span>
    </td>
    <td>
        <a target="_blank" href="<?php echo $person["WWW"];?>"><?php echo $url;?></a><br>
    </td>
</tr>
<?php
}

if($person["Address"]) {
?>
<tr>
    <td>
        <span class="small-header">office</span>
    </td>
    <td>
        <?php echo $person["Address"];?>
    </td>
</tr>
<?php } ?>
</table>

<?php

    if (count($advisees) + count($alumni)) {
        echo "<table id=\"advisees\" class=\"persondetails\">\n";
        if (count($advisees)){
            echo "<tr><td><span class=\"small-header\">advisees</span></td> <td>" . join(", ",$advisees) . "</td></tr>\n";
        } 
        if (count($alumni)){
            echo "<tr><td><span class=\"small-header\">alumni</span></td> <td>" . join(", ",$alumni) . "</td></tr>\n";
        } 
        echo "</table>\n";
    }

?>


<div id="note">
  <?php echo $person["Note"];?>
</div>

</div>

<div class="clear"></div>

</div>

</div>

<?php if (personHasPapers($db,$id)) { ?>

<div id="publication-section" class="box-shadow papers-listing">
<p class="section-title">publications with s3lab
</p>
<?php
$text = getPublications($db,$id);
echo $text;
?>
</div>

<?php } ?>

</div>
</div>

</body>
</html>


