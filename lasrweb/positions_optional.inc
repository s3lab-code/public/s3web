<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<ul>
<li>
  <b>Robotics &amp; Control Systems</b>:
  ROS,
  Autonomous Driving Systems (Autoware, Baidu Apollo),
  Drone Control Systems (ArduPilot, PX4),
  PID Controller
  <!--PLC Programming-->
</li>
<li>
  <b>Program Analysis &amp; Software Testing</b>:
  Rust, LLVM, GCC,
  QEMU, Pin, Valgrind,
  Fuzzing (AFL, LibFuzzer),
  Symbolic Execution (KLEE, S2E)
</li>
<li>
  <b>Hacking &amp; Reverse Engineering</b>:
  CTF, IDA, Ghidra, Detours, DynamoRIO, Dyninst
</li>
<li>
  <b>Embedded Systems &amp; Hardware Security</b>:
  Verilog, FPGA, Device IO (MMIO, DMA, GPIO),
  Memory Protection (MMU, MPU, Intel MPX),
  Trusted Execution Environments (Intel SGX, AMD SEV, ARM TrustZone),
  Execution Tracing and Debugging (Intel PT, ARM ETM, JTAG)
  <!--Hardware Vulnerabilities (Rowhammer, Spectre, Meltdown)-->
</li>
<!--<li>
  <b>Operating System Tracing</b>:
  Linux Audit, Windows ETW, SystemTap, DTrace, eBPF, kprobes
</li>-->
</ul>
