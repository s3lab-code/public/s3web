<?php
include 'header.inc';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
include '../tools.inc';
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(isset($_GET["uid"])) {
$id = $_GET["uid"];
} else {
$id="";
}

$var_array = array('name', 'title', 'description', 'details', 'tag');
foreach ($var_array as $i => $varname){
if(isset($_POST[$varname])){
$$varname = $_POST[$varname];
} else {
$$varname = "";
}
}


$error = false;
if(isset($_POST["submit"])){

// Need some error checking for required fields (maybe on clicking submit button using java script)


if ($tag) {
    $tag = "'$tag'";
} else {
    $tag = "NULL";
}

if($_POST["action"] == "Save changes"){
#$query = "UPDATE Course SET ".
#     "Name='$name', ".
#     "Title='$title', ".
#     "Description='" . addslashes($description) . "', ".
#     "Details='" . addslashes($details) . "', ".
#     "Tag=$tag" .
#     "WHERE uid=$id";
$query = "UPDATE Course SET ".
     "Name='$name', ".
     "Title='$title', ".
     "Description='" . addslashes($description) . "', ".
     "Details='" . addslashes($details) . "', ".
     "Tag=$tag" .
     "WHERE uid=".mysqli_real_escape_string($db, $id);
#$result = mysql_query($query);
$result = mysqli_query($db,$query);
#if(mysql_error()) {
if(mysqli_error($db)) {
$error = true;
#$ERROR_MSG = mysql_error();
$ERROR_MSG = mysqli_error($db);
} else {
$SUCCESS_MSG = "Course information updated successfully!";
// Update paper-area entries. If they are checked but don't exist, insert new entry
// If they are unchecked, but exist, delete that entry
#$result = mysql_query("SELECT * FROM ResearchArea");
$result = mysqli_query($db,"SELECT * FROM ResearchArea");
#while($area = mysql_fetch_assoc($result)) {
while($area = mysqli_fetch_assoc($result)) {
    $aid = $area["uid"];
    if(isset($_POST["area".$aid])) {
        $checked = true;
    } else {
        $checked = false;
    }
    #$existing = mysql_num_rows(mysql_query("SELECT * FROM RelCourseArea WHERE CourseID=$id AND AID=$aid"));
    #$existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelCourseArea WHERE CourseID=$id AND AID=$aid"));
    $existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelCourseArea WHERE CourseID=".mysqli_real_escape_string($db, $id)." AND AID=".mysqli_real_escape_string($db, $aid)));
    if($checked==false && $existing==1) {
        #mysql_query("DELETE FROM RelCourseArea WHERE CourseID=$id AND AID=$aid");
        #mysqli_query($db,"DELETE FROM RelCourseArea WHERE CourseID=$id AND AID=$aid");
        mysqli_query($db,"DELETE FROM RelCourseArea WHERE CourseID=".mysqli_real_escape_string($db, $id)." AND AID=".mysqli_real_escape_string($db, $aid));
    } else if($checked==true && $existing==0) {
        #mysql_query("INSERT INTO RelCourseArea VALUES($id,$aid)");
        mysqli_query($db,"INSERT INTO RelCourseArea VALUES($id,$aid)");
    }
    #if(mysql_error()) {
    if(mysqli_error($db)) {
        $error = true;
        #$ERROR_MSG = mysql_error();
        $ERROR_MSG = mysqli_error($db);
    }
}
}

} else {
$query = "INSERT INTO Course(Name,Title,Description,Details,Tag) VALUES ".
     "('$name','$title','$description','$details',$tag);";
#mysql_query($query);
mysqli_query($db,$query);
  #$id = mysql_insert_id();
  $id = mysqli_insert_id($db);
#if(mysql_error()) {
if(mysqli_error($db)) {
$error = true;
#$ERROR_MSG = mysql_error();
$ERROR_MSG = mysqli_error($db);
} else {
$SUCCESS_MSG = "Course added successfully!";
}
}

if($_FILES["picture"]["name"]!="") {
    $filename = $_FILES["picture"]["tmp_name"];
    $tempname = $_FILES["picture"]["tmp_name"];

    $image = new Imagick($filename);
    $image->setImageFormat("png");
    $image->thumbnailImage(500,500,true);

    $data = $image->getImageBlob();
    $data = addslashes($data);

    #$update_query = "UPDATE Course SET Picture='$data' WHERE uid=$id";
    $update_query = "UPDATE Course SET Picture='$data' WHERE uid=".mysqli_real_escape_string($db, $id);
    #mysql_query($update_query);
    mysqli_query($db,$update_query);
    #if(mysql_error()) {
    if(mysqli_error($db)) {
        $error = true;
        #$ERROR_MSG = mysql_error();
        $ERROR_MSG = mysqli_error($db);
    }
}

}

#$result = mysql_query("SELECT * from Course where uid=$id;");
$result = mysqli_query($db,"SELECT * from Course where uid=$id;");

#if($result && mysql_num_rows($result)>0) {
if($result && mysqli_num_rows($result)>0) {
#$course = mysql_fetch_assoc($result);
$course = mysqli_fetch_assoc($result);
$name = $course["Name"];
$title = $course["Title"];
$description = $course["Description"];
$tag = $course["Tag"];
$details = $course["Details"];
$action = "Save changes";
$label = "edit";
$picture = $course["Picture"];
} else {
$id = "";
$action = "Add course";
$label = "new";
$picture = NULL;
}

?>

<html>
<head>
<title>S3 Lab - Software & Systems Security Laboratory</title>
<link href="../global.css" rel="stylesheet" type="text/css">
<link href="internal-form.css" rel="stylesheet" type="text/css">

<?php INCLUDE_TINYMCE() ?>
</head>

<body>
<div id="internal-form-page" class="page-container">

<div id="title">
<a href=".."><img src="../pics/title.png" style="width:100%"></a>
</div>

<div id="menu-stack">
::
<a href="index.php">maintenance</a>
&rsaquo;&rsaquo;
<a href="show_courses.php">all courses</a>
&rsaquo;&rsaquo;
</div>

<div id="navigation">
view all
<a href="show_news.php">news</a>
::
<a href="show_areas.php">areas</a>
::
<a href="show_users.php">users</a>
::
<a href="show_pubs.php">publications</a>
::
<a href="show_projects.php">projects</a>
</div>

<?php
if($error) {
printErrorFrame($ERROR_MSG);
} else if(isset($SUCCESS_MSG)) {
printSuccessFrame($SUCCESS_MSG);
}
?>

<div id="form">
<p class="section-title"><?php echo $label; ?> course</p>

<p>
<?php
$action_suffix = ($id!=""?"?uid=".$id:"");
?>
<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']).$action_suffix;?>" enctype="multipart/form-data">
<input type=hidden id="action" name="action" value="<?php echo $action; ?>">

<table border=0>
<tr>
<td class="single-row-header">Course Name (CS xxxx)</td>
<td>
<input type="text" name="name" class="long-text-input" 
value="<?php echo $name;?>" />
</td>
</tr>

<tr>
<td class="single-row-header">Course Title</td>
<td>
<input type="text" name="title" class="long-text-input" 
value="<?php echo $title;?>" />
</td>
</tr>

<tr>
<td class="single-row-header">Short Tag (one word)</td>
<td>
<input type="text" name="tag" class="long-text-input" 
value="<?php echo $tag;?>" />
</td>
</tr>

<tr>
<td class="multi-row-header">Short Description (one paragraph)</td>
<td>
<textarea name="description" class="textarea">
<?php echo $description ?>
</textarea><br/>
</tr>

<tr>
<td class="multi-row-header">More Details</td>
<td>
<textarea name="details" class="textarea">
<?php echo $details ?>
</textarea><br/>
</tr>

<tr>
<td class="multi-row-header">
Picture (optional)<br/>
</td>
<td>
<input type="file" name="picture" value="" ><br/>
<span style="font-size:9pt">
Please ensure that (a) the picture is at least 500x500 and (b) is square; if it isn't, the script will make it square, and you may not like the results.
</span>
</td>
</tr>

<?php
if($picture != ""){
echo '<tr>'."\n";
echo '<td></td>'."\n";
echo '<td class="single-row-header">'."\n";
echo '<img src="../download.php?uid='.$id.'&logo=1&'.rand().'" alt="" height="100" width="100">'."\n";
echo '</td>'."\n";
echo '</tr>'."\n";
}
?>
<tr>
<td>
</td>
<td>
<?php getAreaTable($db,$id,"Course"); ?>
</td>
</tr>
<tr>
<td>
</td>
<td>
<input type="submit" name="submit" value="<?php echo $action; ?>" />
</td>
</tr>

</table>

</form>

</div>

</div>
</body>
</html>

