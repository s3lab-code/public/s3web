<?php
include 'header.inc';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
include '../tools.inc';
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(isset($_GET["uid"])) {
$id = $_GET["uid"];
} else {
$id="";
}

$var_array = array('name', 'tag');
foreach ($var_array as $i => $varname){
if(isset($_POST[$varname])){
$$varname = $_POST[$varname];
} else {
$$varname = "";
}
}


$error = false;
if(isset($_POST["submit"])){

// Need some error checking for required fields (maybe on clicking submit button using java script)


if ($tag) {
    $tag = "'$tag'";
} else {
    $tag = "NULL";
}

if($_POST["action"] == "Save changes"){
#$query = "UPDATE ResearchArea SET ".
#     "Name='$name', ".
#     "Tag=$tag" .
#     "WHERE uid=$id";
$query = "UPDATE ResearchArea SET ".
     "Name='$name', ".
     "Tag=$tag" .
     "WHERE uid=".mysqli_real_escape_string($db, $id);
#$result = mysql_query($query);
$result = mysqli_query($db,$query);
#if(mysql_error()) {
if(mysqli_error($db)) {
$error = true;
#$ERROR_MSG = mysql_error();
$ERROR_MSG = mysqli_error($db);
} else {
$SUCCESS_MSG = "Area information updated successfully!";
}

} else {
$query = "INSERT INTO ResearchArea(Name,Tag) VALUES ".
     "('$name',$tag);";

#mysql_query($query);
mysqli_query($db,$query);
  #$id = mysql_insert_id();
  $id = mysqli_insert_id($db);
#if(mysql_error()) {
if(mysqli_error($db)) {
$error = true;
#$ERROR_MSG = mysql_error();
$ERROR_MSG = mysqli_error($db);
} else {
$SUCCESS_MSG = "Area added successfully!";
}
}

}

#$result = mysql_query("SELECT * from ResearchArea where uid=$id;");
$result = mysqli_query($db,"SELECT * from ResearchArea where uid=$id;");

#if($result && mysql_num_rows($result)>0) {
if($result && mysqli_num_rows($result)>0) {
#$area = mysql_fetch_assoc($result);
$area = mysqli_fetch_assoc($result);
$name = $area["Name"];
$tag = $area["Tag"];
$action = "Save changes";
$label = "edit";
} else {
$id = "";
$action = "Add area";
$label = "new";
}

?>

<html>
<head>
<title>S3 Lab - Software & Systems Security Laboratory</title>
<link href="../global.css" rel="stylesheet" type="text/css">
<link href="internal-form.css" rel="stylesheet" type="text/css">

<?php INCLUDE_TINYMCE() ?>
</head>

<body>
<div id="internal-form-page" class="page-container">

<div id="title">
<a href=".."><img src="../pics/title.png" style="width:100%"></a>
</div>

<div id="menu-stack">
::
<a href="index.php">maintenance</a>
&rsaquo;&rsaquo;
<a href="show_areas.php">all areas</a>
&rsaquo;&rsaquo;
</div>

<div id="navigation">
view all
<a href="show_news.php">news</a>
::
<a href="show_users.php">users</a>
::
<a href="show_pubs.php">publications</a>
::
<a href="show_projects.php">projects</a>
::
<a href="show_courses.php">courses</a>
</div>

<?php
if($error) {
printErrorFrame($ERROR_MSG);
} else if(isset($SUCCESS_MSG)) {
printSuccessFrame($SUCCESS_MSG);
}
?>

<div id="form">
<p class="section-title"><?php echo $label; ?> area</p>

<p>
<?php
$action_suffix = ($id!=""?"?uid=".$id:"");
?>
<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']).$action_suffix;?>" enctype="multipart/form-data">
<input type=hidden id="action" name="action" value="<?php echo $action; ?>">

<table border=0>
<tr>
<td class="single-row-header">Area Name </td>
<td>
<input type="text" name="name" class="long-text-input" 
value="<?php echo $name;?>" />
</td>
</tr>

<tr>
<td class="single-row-header">Shortname (one word) </td>
<td>
<input type="text" name="tag" class="long-text-input" 
value="<?php echo $tag;?>" />
</td>
</tr>

<tr>
<td>
</td>
<td>
<input type="submit" name="submit" value="<?php echo $action; ?>" />
</td>
</tr>

</table>

</form>

</div>

</div>
</body>
</html>

