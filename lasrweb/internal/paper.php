<?php
include 'header.inc'
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.o

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php

include '../tools.inc';
error_reporting(E_ALL);
ini_set('display_errors', 1);

# Argh, hate this bullshit, workaround for a feature PHP shouldn't have
# ever had
function SAFESLASHES($string) {
    #if (get_magic_quotes_gpc()) {
    #    return $string;
    #} else {
        return addslashes($string);
    #}
}

if(isset($_POST["paper-id"])) {
        $id = $_POST["paper-id"];
} else if(isset($_GET["uid"])) {
        $id = $_GET["uid"];
} else {
        $id="";
}

$error=false;
if(isset($_POST["submit"])) {
            $postTitle = SAFESLASHES($_POST["title"]);
            $postNumAuthors = $_POST["numAuthors"];
            $authorList = "";
            $postAuthor = array();
            $postAuthorText = array();
            for($i=0;$i<$postNumAuthors;$i++) {
                    $key="author".$i;
                    $key2="authorText".$i;
                    $postAuthor[$i]=$_POST[$key];
                    $postAuthorText[$i]=$_POST[$key2];
                    # This is now OK, since they may have an author
                    # string instead
                    # if($postAuthor[$i]=="") {
                    #	$error = true;
                    #	$ERROR_MSG = "Invalid author selection";
                    #}
            }
            $authorList = getAuthorList($postNumAuthors,$postAuthor,$postAuthorText);
            $postType = $_POST["type"];
            $postFullname = SAFESLASHES($_POST["fullname"]);
            $postShortname = SAFESLASHES($_POST["shortname"]);
            $postYear = $_POST["year"];
            $postMonth = $_POST["month"];
            $postLocation = SAFESLASHES($_POST["location"]);
            $postVolume = $_POST["volume"];
            $postNumber = $_POST["number"];
            $postArtType = $_POST["arttype"];
            $postInstitution = $_POST["institution"];
            $postPage1 = $_POST["page1"];
            $postPage2 = $_POST["page2"];
            $postDOI = SAFESLASHES($_POST["doi"]);
            $postCopyright = SAFESLASHES($_POST["copyright"]);
            $postCode = SAFESLASHES($_POST["code"]);
            $postWeb = SAFESLASHES($_POST["web"]);
            $postTag = SAFESLASHES($_POST["tag"]);
            if ($postTag) {
                $postTag = "'$postTag'";
            } else {
                $postTag = "NULL";
            }
            if(isset($_POST["award"])) {
                    $postAward = 1;
            } else {
                    $postAward = 0;
            }
            if($postPage2 == "") {
                    $setPages = $postPage1;
            } else {
                    $setPages = $postPage1. "-" . $postPage2;
            }
            $postAbstract = SAFESLASHES($_POST["abstract"]);
            $postBibEntry = SAFESLASHES($_POST["bibentry"]);
            
            if(!$error) {
                    if($_POST["action"]=="Save changes") {
                            #$update_query = "UPDATE Paper SET Title='$postTitle', AuthorList='$authorList', Type='$postType', Venue_full='$postFullname', Venue_short='$postShortname', Year='$postYear', Month='$postMonth', Location='$postLocation', VolumeNum='$postVolume', ArtType='$postArtType', Institution='$postInstitution', Number='$postNumber', PageNum='$setPages', DOI='$postDOI', Copyright='$postCopyright', Code='$postCode', Web='$postWeb', Tag=$postTag, Abstract='$postAbstract', BibEntry='$postBibEntry', AwardPaper=$postAward WHERE uid=$id;";
                            $update_query = "UPDATE Paper SET Title='$postTitle', AuthorList='$authorList', Type='$postType', Venue_full='$postFullname', Venue_short='$postShortname', Year='$postYear', Month='$postMonth', Location='$postLocation', VolumeNum='$postVolume', ArtType='$postArtType', Institution='$postInstitution', Number='$postNumber', PageNum='$setPages', DOI='$postDOI', Copyright='$postCopyright', Code='$postCode', Web='$postWeb', Tag=$postTag, Abstract='$postAbstract', BibEntry='$postBibEntry', AwardPaper=$postAward WHERE uid=".mysqli_real_escape_string($db, $id).";";
                            #mysql_query($update_query);
                            mysqli_query($db,$update_query);
                            #if(mysql_error()) {
                            if(mysqli_error($db)) {
                                    $error = true;
                                    #$ERROR_MSG = mysql_error();
                                    $ERROR_MSG = mysqli_error($db);
                            } else {
                                    $SUCCESS_MSG = "Publication updated successfully!";
                            }
                            if(!$error) {
                                    // deleting all paper-author entries.. we will add all the relevant ones later
                                    #$deletePaperAuthor_query = "DELETE FROM RelPaperAuthor WHERE PaperID=$id";
                                    $deletePaperAuthor_query = "DELETE FROM RelPaperAuthor WHERE PaperID=".mysqli_real_escape_string($db, $id);
                                    #mysql_query($deletePaperAuthor_query);
                                    mysqli_query($db,$deletePaperAuthor_query);
                            }
                    } else {
                            $insert_query = "INSERT INTO Paper(Title,AuthorList,Type, Venue_full, Venue_short, Year, Month,Location,VolumeNum, Number, ArtType, Institution, PageNum,DOI,Copyright,Code,Web,Tag,Abstract,BibEntry,AwardPaper) VALUES ('$postTitle','$authorList','$postType','$postFullname','$postShortname','$postYear','$postMonth','$postLocation','$postVolume','$postNumber','$postArtType','$postInstitution','$setPages','$postDOI','$postCopyright','$postCode','$postWeb',$postTag,'$postAbstract','$postBibEntry',$postAward)";
                            #mysql_query($insert_query);
                            mysqli_query($db,$insert_query);
                            #if(mysql_error()) {
                            if(mysqli_error($db)) {
                                    $error = true;
                                    #$ERROR_MSG = mysql_error();
                                    $ERROR_MSG = mysqli_error($db);
                            } else {
                                    $SUCCESS_MSG = "Publication added successfully!";
                            }
                            #$id = mysql_insert_id();
                            $id = mysqli_insert_id($db);
                    }
            }
    
            if($id>0 && !$error) {
                    // Since we deleted all paper-author entries, we will 
                    // now add the submitted ones. Note that we pointedly
                    // ignore the authors who are given by name only
                    for($i=0;$i<$postNumAuthors;$i++) {	
                            $authorID = $postAuthor[$i];
                            if ($authorID) {
                                #mysql_query("INSERT INTO RelPaperAuthor VALUES($id,$authorID)");
                                mysqli_query($db,"INSERT INTO RelPaperAuthor VALUES($id,$authorID)");
                            }
                    }

                    // Update paper-area entries. If they are checked but don't exist, insert new entry
                    // If they are unchecked, but exist, delete that entry
                    #$result = mysql_query("SELECT * FROM ResearchArea");
                    $result = mysqli_query($db,"SELECT * FROM ResearchArea");
                    #while($area = mysql_fetch_assoc($result)) {
                    while($area = mysqli_fetch_assoc($result)) {
                            $aid = $area["uid"];
                            if(isset($_POST["area".$aid])) {
                                    $checked = true;
                            } else {
                                    $checked = false;
                            }
                            #$existing = mysql_num_rows(mysql_query("SELECT * FROM RelPaperArea WHERE PaperID=$id AND AID=$aid"));
                            #$existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelPaperArea WHERE PaperID=$id AND AID=$aid"));
                            $existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelPaperArea WHERE PaperID=".mysqli_real_escape_string($db, $id)." AND AID=".mysqli_real_escape_string($db, $aid)));
                            if($checked==false && $existing==1) {
                                    #mysql_query("DELETE FROM RelPaperArea WHERE PaperID=$id AND AID=$aid");
                                    #mysqli_query($db,"DELETE FROM RelPaperArea WHERE PaperID=$id AND AID=$aid");
                                    mysqli_query($db,"DELETE FROM RelPaperArea WHERE PaperID=".mysqli_real_escape_string($db, $id)." AND AID=".mysqli_real_escape_string($db, $aid));
                            } else if($checked==true && $existing==0) {
                                    #mysql_query("INSERT INTO RelPaperArea VALUES($id,$aid)");
                                    mysqli_query($db,"INSERT INTO RelPaperArea VALUES($id,$aid)");
                            }
                            #if(mysql_error()) {
                            if(mysqli_error($db)) {
                                    $error = true;
                                    #$ERROR_MSG = mysql_error();
                                    $ERROR_MSG = mysqli_error($db);
                            }

                    }
                    
                    // Same for projects
                    #$result = mysql_query("SELECT uid FROM Project");
                    $result = mysqli_query($db,"SELECT uid FROM Project");
                    #while($project = mysql_fetch_assoc($result)) {
                    while($project = mysqli_fetch_assoc($result)) {
                            $projid = $project["uid"];
                            if(isset($_POST["project".$projid])) {
                                    $checked = true;
                            } else {
                                    $checked = false;
                            }
                            #$existing = mysql_num_rows(mysql_query("SELECT * FROM RelProjectPaper WHERE PaperID=$id AND ProjectID=$projid"));
                            #$existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelProjectPaper WHERE PaperID=$id AND ProjectID=$projid"));
                            $existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelProjectPaper WHERE PaperID=".mysqli_real_escape_string($db, $id)." AND ProjectID=".mysqli_real_escape_string($db, $projid)));
                            if($checked==false && $existing==1) {
                                    #mysql_query("DELETE FROM RelProjectPaper WHERE PaperID=$id AND ProjectID=$projid");
                                    #mysqli_query($db,"DELETE FROM RelProjectPaper WHERE PaperID=$id AND ProjectID=$projid");
                                    mysqli_query($db,"DELETE FROM RelProjectPaper WHERE PaperID=".mysqli_real_escape_string($db, $id)." AND ProjectID=".mysqli_real_escape_string($db, $projid));
                            } else if($checked==true && $existing==0) {
                                    #mysql_query("INSERT INTO RelProjectPaper SET PaperID=$id, ProjectID=$projid");
                                    mysqli_query($db,"INSERT INTO RelProjectPaper SET PaperID=$id, ProjectID=$projid");
                            }
                            #if(mysql_error()) {
                            if(mysqli_error($db)) {
                                    $error = true;
                                    #$ERROR_MSG = mysql_error();
                                    $ERROR_MSG = mysqli_error($db);
                            }

                    }

                    if($_FILES["pdf-attach"]["name"]!="") {
                            $tempname = $_FILES["pdf-attach"]["tmp_name"];
                            $fp = fopen($tempname, "r");
                            $content = fread($fp, filesize($tempname));
                            $content = addslashes($content);
                            fclose($fp);

                            $dirname = dirname($tempname);
                            $basename = basename($tempname);
                            $thumbnailname = $dirname."/".$basename.".jpg";
                            #$shell_query = escapeshellcmd("env PATH=/usr/bin convert ".$tempname."[0] ".$thumbnailname);
                            #$output = shell_exec($shell_query);
                            $im = new Imagick();
                            $im->readimage($tempname."[0]");
                            $im->setImageFormat("jpeg");
                            $im->writeImage($thumbnailname);
                            $im->clear();
                            $im->destroy();
                            $fp = fopen($thumbnailname, "r");
                            $thumbnail_content = fread($fp, filesize($thumbnailname));
                            $thumbnail_content = addslashes($thumbnail_content);
                            fclose($fp);
                            
                            #$updatePdf_query = "UPDATE Paper SET Pdf='$content',Thumbnail='$thumbnail_content' WHERE uid=$id";
                            $updatePdf_query = "UPDATE Paper SET Pdf='$content',Thumbnail='$thumbnail_content' WHERE uid=".mysqli_real_escape_string($db, $id);
                            #mysql_query($updatePdf_query);
                            mysqli_query($db,$updatePdf_query);
                    } elseif( key_exists( 'delete-pdf', $_POST ) ) {
		      	    #mysql_query( "UPDATE Paper SET Pdf=NULL WHERE uid=$id" );
		      	    #mysqli_query( $db,"UPDATE Paper SET Pdf=NULL WHERE uid=$id" );
		      	    mysqli_query( $db,"UPDATE Paper SET Pdf=NULL WHERE uid=".mysqli_real_escape_string($db, $id) );
		    }

                    if($_FILES["slides-attach"]["name"]!="") {
                            $filename = $_FILES["slides-attach"]["name"];
                            $tempname = $_FILES["slides-attach"]["tmp_name"];
                            $extension = strtolower(substr(strrchr($filename, '.'), 1));
                            //$new_name = "pubs/paper".$id.".".$extension;
                            //$shell_query="cp ".$tempname." ../".$new_name;
                            $fp = fopen($tempname, "r");
                            $content = fread($fp, filesize($tempname));
                            $content = addslashes($content);
                            fclose($fp);

                            #$updateSlides_query = "UPDATE Paper SET slides='$content',slides_type='$extension' WHERE uid=$id";
                            $updateSlides_query = "UPDATE Paper SET slides='$content',slides_type='$extension' WHERE uid=".mysqli_real_escape_string($db, $id);
                            //shell_exec($shell_query);
                            #mysql_query($updateSlides_query);
                            mysqli_query($db,$updateSlides_query);
                            if(mysqli_error($db)) {
                                    $error = true;
                                    #$ERROR_MSG = mysql_error();
                                    $ERROR_MSG = mysqli_error($db);
                            }
                    } elseif( key_exists( 'delete-slides', $_POST ) ) {
		      	    #mysql_query( "UPDATE Paper SET slides=NULL WHERE uid=$id" );
		      	    #mysqli_query( $db,"UPDATE Paper SET slides=NULL WHERE uid=$id" );
		      	    mysqli_query( $db,"UPDATE Paper SET slides=NULL WHERE uid=".mysqli_real_escape_string($db, $id) );
		    }

            }
} 

# Some temporary debugging!
if (isset($_POST['loadbib'])) {
    $_GET['library']=1;
    include '../bibtexbrowser/bibtexbrowser.php';

    # Argh, I hate the magic quote crap
    $bibentry = stripslashes($_POST['bibentry']);
    
    # Not happy to be writing this to a temp file, but it seems like the 
    # easiest way to use the code
    $temp = tmpfile();
    fwrite($temp, $bibentry);
    fflush($temp);

    $meta_data = stream_get_meta_data($temp);
    $filename = $meta_data["uri"];
    $bibdb = zetDB($filename);

    $entries = $bibdb->getEntries();
    $keys = array_keys($entries);

    if (count($keys) != 1) {
        die("Didn't get a bib file with exatly 1 entry\n");
    }

    $key = $keys[0];
    $entry = $entries[$key];

    $title = $entry->getTitle();
    $year = $entry->getYear();

    $dateinfo = date_parse($entry->getField("month"));
    $month = $dateinfo['month'];

    $location = $entry->getField("location");
    $volume = $entry->getField("volume");
    $number = $entry->getField("number");
    $arttype = $entry->getField("type");
    $pages = $entry->getField("pages");
    $doi = $entry->getField("doi");

    switch ($entry->getType()) {
    case "inproceedings":
        $type = "Conference";
        $fullname = $entry->getField("booktitle");
        break;
    case "article":
        $type = "Journal";
        $fullname = $entry->getField("journal");
        break;
    case "mastersthesis":
        $type = "MSThesis";
        $institution = $entry->getField("school");
        break;
    case "phdthesis":
        $type = "PHDThesis";
        $institution = $entry->getField("school");
        break;
    case "techreport":
        $type = "TR";
        $institution = $entry->getField("institution");
        break;
    default:
        $type = "Other";
        $fullname = $entry->getField("booktitle");
        break;
    }

    # Hackery to guess short name
    if (preg_match('/^(.*) \((\w+)\)$/',$fullname,$matches)) {
        $fullname = $matches[1];
        $shortname = $matches[2];
    } else {
        $shortname = "";
    }

    if( preg_match( '/([0-9]+)[- ]+([0-9]+)/', $pages, $matches ) ) {
    	$page1 = $matches[ 1 ];
    	$page2 = $matches[ 2 ];
    } else {
        $page1 = $page2 = "";
    }

    $bibauthors = $entry->getRawAuthors();
    $authors = array();
    $authortext = array();
    $numAuthors = 0;
    $firstauthor = "";
    foreach ($bibauthors as $bibauthor) {
        if (!$firstauthor) {
            $firstauthor = $bibauthor;
        }
        # Really crude way to match authors
        #$result = mysql_query("SELECT uid from Person where LOWER(Full_name) = LOWER(\"$bibauthor\")");
        $result = mysqli_query($db,"SELECT uid from Person where LOWER(Full_name) = LOWER(\"$bibauthor\")");
        #if (mysql_num_rows($result) > 0) {
        if (mysqli_num_rows($result) > 0) {
            #$row = mysql_fetch_assoc($result);
            $row = mysqli_fetch_assoc($result);
            array_push($authors,$row['uid']);
            array_push($authortext,NULL);
        } else {
            array_push($authors,NULL);
            array_push($authortext,$bibauthor);
        }
        $numAuthors++;
    }

    # Set tag
    if (!empty($firstauthor)) {
        $lastname = substr(strstr($firstauthor," "),1);
    } else {
        $lastname = "";
    }

    if ($year) {
        $shortyear = sprintf("%02d",$year % 100); 
    } else {
        $shortyear = "";
    }

    if ($shortname && $lastname && $shortyear != "") {
        $tag = $lastname . "-" . $shortname . $year;
    } elseif ($type == "MSThesis" && $lastname) {
        $tag = $lastname . "-thesis";
    } elseif ($type == "PHDThesis" && $lastname) {
        $tag = $lastname . "-dissertation";
    } elseif ($type == "TR" && $lastname && $shortyear != "") {
        $tag = $lastname . "-tr" . $year;
    } else {
        $tag = "";
    }
    $tag = strtolower($tag);

    $award = 0;
    $abstract = "";
    $copyright = "";
    $code = "";
    $web = "";

    # This has to happen late; when called, the tempfile gets removed
    fclose($temp);

    $SUCCESS_MSG = "WARNING: Paper *not* saved - be sure to save it!";
    if ($id == -1) { 
        $action = "Add paper";
    } else {
        $action = "Save changes";
    }
} else {

#$result = mysql_query("SELECT * from Paper where uid=$id;");
$result = mysqli_query($db,"SELECT * from Paper where uid=$id;");
#if($result && mysql_num_rows($result)>0) {
if($result && mysqli_num_rows($result)>0) {
        #$paper = mysql_fetch_assoc($result);
        $paper = mysqli_fetch_assoc($result);
        $title = $paper["Title"];
        $authorids = getAuthorIDs($paper["AuthorList"]);
	$numAuthors = count($authorids);
        $type = $paper["Type"];
        $fullname = $paper["Venue_full"];
        $shortname = $paper["Venue_short"];
        $year = $paper["Year"];
        $month = $paper["Month"];
        $location = $paper["Location"];
        $volume = $paper["VolumeNum"];
        $number = $paper["Number"];
        $arttype = $paper["ArtType"];
        $institution = $paper["Institution"];
        $doi = $paper["DOI"];
        $copyright = $paper["Copyright"];
        $code = $paper["Code"];
        $web = $paper["Web"];
        $tag = $paper["Tag"];
        $abstract = $paper["Abstract"];
        $bibentry = $paper["BibEntry"];
        $action = "Save changes";
        $label = "edit";
	$pages = explode("-",$paper["PageNum"]);
	$page1 = $pages[0];
	if(count($pages) >1) {
		$page2 = $pages[1];
	} else {
		$page2 = "";
	}
	$award = $paper["AwardPaper"];
	$has_pdf = $paper["Pdf"] != NULL;
	$has_slides = $paper["slides"] != NULL;
} else {
        if(isset($_POST["submit"])) {
                $title = $_POST["title"];
		$numAuthors = $postNumAuthors;
		for($i=0; $i < $postNumAuthors; $i++) {
			$key = "author". $i;
			$key2 = "authorText". $i;
                	$authors[$i] = $_POST[$key];
                	$authortext[$i] = $_POST[$key2];
		}
                $abstract = $_POST["abstract"];
                $bibentry = $_POST["bibentry"];
                $type = $_POST["type"];
                $fullname = $_POST["fullname"];
                $shortname = $_POST["shortname"];
                $year = $_POST["year"];
                $month = $_POST["month"];
                $location = $_POST["location"];
                $volume = $_POST["volume"];
                $number = $_POST["number"];
                $arttype = $_POST["arttype"];
                $institution = $_POST["institution"];
                $doi = $_POST["doi"];
                $copyright = $_POST["copyright"];
                $code = $_POST["code"];
                $web = $_POST["web"];
                $tag = $_POST["tag"];
        	$page1 = $_POST["page1"];
        	$page2 = $_POST["page2"];
		if(isset($_POST["award"])) {
		    $award = $_POST["award"];
		} else {
		    $award = 0;
		}
		$has_pdf = $_FILES["pdf-attach"]["name"] != "";
		$has_slides = $_FILES["slides-attach"]["name"] != "";
        } else {
                $title = "";
		$numAuthors = 1;
                $type = "Conference";
                $fullname = "";
                $shortname = "";
                $year = "";
                $month = "";
                $location = "";
                $volume = "";
                $number = "";
                $arttype = "";
                $institution = "";
                $doi = "";
                $copyright = "";
                $code = "";
                $web = "";
                $tag = "";
                $abstract = "";
                $bibentry = "";
		$venue = "";
		$page1 = "";
		$page2 = "";
		$award = 0;
                $authors = array();
                $authortext = array();
		$has_pdf = 0;
		$has_slides = 0;
	}

        $id = -1;
        $action = "Add paper";
        $label = "new";
}
}

?>

<html>
<head>
    <title>S3 Lab - Software & Systems Security Laboratory</title>
    <link href="../global.css" rel="stylesheet" type="text/css">
    <link href="internal-form.css" rel="stylesheet" type="text/css">
    <!--<script type="text/javascript" src="../javascript/prototype.js"></script>
    <script type="text/javascript" src="../javascript/scriptaculous.js"></script>-->
    <script type="text/javascript" src="../tools.js"></script>
    <script type="text/javascript">
	function init() {
		document.getElementById('num').value=<?php echo $numAuthors;?>;
		//Sortable.create('allAuthors',{tag:'span'});
	}
    </script>
<style type="text/css">
span.handle { cursor: move; }
</style>

<?php INCLUDE_TINYMCE("textarea.abstract"); ?>

</head>

<body onload="init();">
<div id="internal-form-page" class="page-container">
<?php
//print_r($_POST); 
//echo "<br/>";
//echo $insert_query;
//echo "file: ".$_FILES["pdf-attach"]["name"]."<br/>"; 
//echo "shellQuery: ".$shell_query."<br/>";
//echo "updatePdf_query: ".$updatePdf_query."<br/>";
//echo $output;
?>

<div id="title">
<a href=".."><img src="../pics/title.png" style="width:100%"></a>
</div>

<div id="menu-stack">
::
<a href="index.php">maintenance</a>
&rsaquo;&rsaquo;
<a href="show_pubs.php">all pubications</a>
&rsaquo;&rsaquo;
</div>

<div id="navigation">
view all
<a href="show_news.php">news</a>
::
<a href="show_areas.php">areas</a>
::
<a href="show_users.php">users</a>
::
<a href="show_projects.php">projects</a>
::
<a href="show_courses.php">courses</a>
</div>

<?php 
if($error) {
	printErrorFrame($ERROR_MSG); 
} else if(isset($SUCCESS_MSG)) {
	printSuccessFrame($SUCCESS_MSG);
}
?>

<div id="form" class="box-shadow">

<p class="section-title"><?php echo $label; ?> publication

<p>
<?php 
$action_suffix = ($id!=""?"?uid=".$id:"");
?>
<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']).$action_suffix;?>" enctype="multipart/form-data">
<table border=0>
<tr>
<td width="100px">Title of paper</td>
<td><input type="text" name="title" size=90 value="<?php echo $title; ?>" /></td>
</tr>

<tr>
<td>Authors</td>
<td>
<input type=hidden id="num" name="numAuthors" value="<?php echo $numAuthors; ?>">
<input type=hidden id="paper-id" name="paper-id" value="<?php echo $id; ?>">
<input type=hidden id="action" name="action" value="<?php echo $action; ?>">

<div id="allAuthors">
<?php
for($i=0; $i<30; $i++) {
    $authid = "";
    $authtext = "";
    if (isset($authorids)) {
        # This happens if we read the record from the DB
        if (isset($authorids[$i])) {
            if (is_numeric($authorids[$i])) {
                $authid = $authorids[$i];
            } else{
                $authtext = $authorids[$i];
            }
        }
        # Otherwise, off the end of the list, leave 'em blank
    } else {
        # Comes from last submission of this form
        if (isset($authors[$i])) {
            $authid = $authors[$i];
        } else if (isset($authortext[$i])) {
            $authtext = $authortext[$i];
        }
    }
    getAuthorDropdown($db,$i,$authid,$authtext);
}
?>
<a href="#" class="small-link" onClick="addAuthor();">more authors</a> ::
<a href="#" class="small-link" onClick="lessAuthors();">fewer authors</a>
</div>
</td>
</tr>

<tr>
<td>Paper type</td>
<td>
<select name="type">
<option value="Conference" <?php echo $type == "Conference"?"selected":"" ?>>Conference</option>
<option value="Journal" <?php echo $type == "Journal"?"selected":"" ?>>Journal</option>
<option value="Workshop" <?php echo $type == "Workshop"?"selected":"" ?>>Workshop</option>
<option value="TR" <?php echo $type == "TR"?"selected":"" ?>>Tech Report</option>
<option value="Magazine" <?php echo $type == "Magazine"?"selected":"" ?>>Magazine</option>
<option value="MSThesis" <?php echo $type == "MSThesis"?"selected":"" ?>>MS Thesis</option>
<option value="PHDThesis" <?php echo $type == "PHDThesis"?"selected":"" ?>>PhD Dissertation</option>
<option value="Other" <?php echo $type == "Other"?"selected":"" ?>>Other</option>
</select>
</td>
</tr>

<tr>
<td>Published in</td>
<td>
Venue full name: <input type="text" name="fullname" value="<?php echo $fullname; ?>" size=50/>
Venue short name: <input type="text" name="shortname" value="<?php echo $shortname; ?>" size=10/>
</td>
</tr>

<tr>
<td>Year</td>
<td>
<input type="text" name="year" value="<?php echo $year; ?>"/>
</td>
</tr>

<tr>
<td>Month</td>
<td>
<select name="month">
<option value="">-</option>
<option value="1" <?php echo $month == 1?"selected":"" ?>>January</option>
<option value="2" <?php echo $month == 2?"selected":"" ?>>February</option>
<option value="3" <?php echo $month == 3?"selected":"" ?>>March</option>
<option value="4" <?php echo $month == 4?"selected":"" ?>>April</option>
<option value="5" <?php echo $month == 5?"selected":"" ?>>May</option>
<option value="6" <?php echo $month == 6?"selected":"" ?>>June</option>
<option value="7" <?php echo $month == 7?"selected":"" ?>>July</option>
<option value="8" <?php echo $month == 8?"selected":"" ?>>August</option>
<option value="9" <?php echo $month == 9?"selected":"" ?>>September</option>
<option value="10" <?php echo $month == 10?"selected":"" ?>>October</option>
<option value="11" <?php echo $month == 11?"selected":"" ?>>November</option>
<option value="12" <?php echo $month == 12?"selected":"" ?>>December</option>
</select>
</td>
</tr>

<tr>
<td>Location</td>
<td>
<input type="text" name="location" value="<?php echo $location; ?>"/> (conference only)
</td>
</tr>


<tr>
<td>Volume</td>
<td>

<input type="text" name="volume" size=3 value="<?php echo $volume;?>" />
(journal or magazine only)
</td>
</tr>

<tr>
<td>Number</td>
<td>

<input type="text" name="number" size=3 value="<?php echo $number;?>" />
(journal, magazine, or TR only)
</td>
</tr>

<tr>
<td>Pages</td>
<td>

<input type="text" name="page1" size=3 value="<?php echo $page1;?>" />
to 
<input type="text" name="page2" size=3 value="<?php echo $page2;?>"/>
(journal or magazine only)
</td>
</tr>

<tr>
<td>Type</td>
<td>

<input type="text" name="arttype" size=25 value="<?php echo $arttype;?>" />
(usually used only for TR)
</td>
</tr>

<tr>
<td>School/Institution</td>
<td>

<input type="text" name="institution" size=25 value="<?php echo $institution;?>" />
(thesis or TR only)
</td>
</tr>

<tr>
<td>DOI</td>
<td>

<input type="text" name="doi" size=25 value="<?php echo $doi;?>" />
(optional)
</td>
</tr>

<tr>
<td>Copyright</td>
<td>

<input type="text" name="copyright" size=80 value="<?php echo $copyright;?>" />
(optional)
</td>
</tr>

<tr>
<td>Code URL</td>
<td>

<input type="text" name="code" size=50 value="<?php echo $code;?>" />
(optional)
</td>
</tr>

<tr>
<td>Webpage URL</td>
<td>

<input type="text" name="web" size=50 value="<?php echo $web;?>" />
(optional)
</td>
</tr>

<tr>
<td>Short tag</td>
<td>

<input type="text" name="tag" size=60 value="<?php echo $tag;?>" />
(optional; for friendly-looking links)
</td>
</tr>

<tr>
<td></td>
<td>
<?php if($award) {?>
	<input type="checkbox" name="award" value="1" checked/>
<?php } else {?>
	<input type="checkbox" name="award" value="0"/>
<?php }?>
Check here if this paper won an award (e.g., best paper, best student paper).
</td>
</tr>


<tr>
<td>Abstract</td>
<td>
<textarea name="abstract" class="abstract" rows=10 cols=80>
<?php echo $abstract; ?>
</textarea>
</td>
</tr>

<tr>
<td>BibTeX entry (will be auto-generated if left blank)</td>
<td>
<textarea name="bibentry" rows=15 cols=75>
<?php echo $bibentry; ?>
</textarea>
<br/>
<input type="submit" name="loadbib" value="Load fields from bib entry" />
</td>
</tr>


<tr>
<td>PDF</td>
<td>
<input type="file" name="pdf-attach" value="PDF"/>
<?php
if( $has_pdf ) {
    echo "or <label><input type=\"checkbox\" name=\"delete-pdf\" value=\"0\" />delete existing PDF</label>\n";
}
?>
</td>
</tr>

<tr>
<td>Slides</td>
<td>
<input type="file" name="slides-attach" value="Slides"/>
<?php
if( $has_slides ) {
    echo "or <label><input type=\"checkbox\" name=\"delete-slides\" value=\"0\" />delete existing slides</label>\n";
}
?>
</td>
</tr>


<tr>
<td>
Areas
</td>
<td>

<?php
getAreaTable($db,$id); 
?>

</td>
</tr>

<tr>
<td>
Projects
</td>
<td>

<?php
getProjTable($db,$id,"Paper"); 
?>

</td>
</tr>

<tr>
<td>
</td>
<td>
<input type="submit" name="submit" value="<?php echo $action; ?>" />
</td>
</tr>

</table>

</form>

</div>

</div>
</body>
</html>


