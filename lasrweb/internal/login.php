<?php

include '../config.inc';

$error = false;
if(isset($_POST["submit"])) {
	if(!isset($_POST["username"]) || $_POST["username"]=="") {
		$error = true;
		$ERROR_MSG = "Please provide a username";
	} else if(!isset($_POST["password"]) || $_POST["password"]=="") {
		$error = true;
		$ERROR_MSG = "Please provide a password";
	} else {
		#$db = mysql_connect($DATABASE_HOST,$_POST["username"],$_POST["password"]);
		$db = mysqli_connect($DATABASE_HOST,$_POST["username"],$_POST["password"],$DATABASE_NAME);
		if(!$db) {
			$error = true;
			$ERROR_MSG = "Invalid credentials";
			session_start();
			session_destroy();
		} else {
			session_start();
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["password"] = $_POST["password"];
			//$_SESSION["db"] = $db;
			?><html><head><meta HTTP-EQUIV="REFRESH" content="0; url=index.php"></head></html>
			<?php exit();
		}
	}
}
if(isset($_GET["action"]) && $_GET["action"] == "logout") {
	session_start();
	session_destroy();
}

include '../tools.inc';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.or
g/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<html>

<head>
    <title>S3 Lab - Software & Systems Security Laboratory</title>
    <link href="../login.css" rel="stylesheet" type="text/css">
</head>

<body>
<!--<table class = "pageLayout hideBoth" id = "pageLayout">
<tr><td class = "layoutColumn center">
-->

<div class = "block" style = ";" id = "Login" >
<?php
if($error) {
        printErrorFrame($ERROR_MSG);
} else if(isset($SUCCESS_MSG)) {
        printSuccessFrame($SUCCESS_MSG);
}
?>
  <div class = "blockContents" >
   <span class = "title">Login</span>

   <span class = "subtitle"></span>
          
   <div class = "content">
   <form class="indexForm" action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
   <div class = "formRow">
	<div class = "formLabel">
      		<div class = "header">Username</div>
        </div>
      	<div class = "formElement">
             	<div class = "field"><input class="inputText" id="login_box" name="username" type="text" /></div>
        </div>
   </div>
   <div class = "formRow">
	<div class = "formLabel">
        	<div class = "header">Password</div>
	</div>
      	<div class = "formElement">
        	<div class = "field"><input class="inputText" tabindex="0" name="password" type="password" /></div>
        </div>
   </div>
   <div class = "formRow">
        <div class = "formLabel">
        	<div class = "header">&nbsp;</div>
        	<div class = "explanation"></div>

        </div>
      	<div class = "formElement">
        	<div class = "field"><input class="flatButton" name="submit" value="Login" type="submit" /></div>
	</div>
   </div>
   </form>
   </div>
  </div>
</div>
<!--</td></tr>
</table>-->
</body>

</html>
