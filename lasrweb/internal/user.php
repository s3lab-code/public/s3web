<?php
include 'header.inc';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
include '../tools.inc';
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(isset($_POST["user-id"])) {
$id = $_POST["user-id"];
} else if(isset($_GET["uid"])) {
$id = $_GET["uid"];
} else {
$id="";
}

$var_array = array('name', 'title','wherenow', 'advisor1', 'advisor2', 'email', 'webpage', 'phone', 'office', 'picture','notes','tag');
foreach ($var_array as $i => $varname){
if(isset($_POST[$varname])){
$$varname = $_POST[$varname];
} else {
$$varname = "";
}
}


$error = false;
if(isset($_POST["submit"])){

// Need some error checking for required fields (maybe on clicking submit button using java script)

if($advisor1 <= 0){
$advisor1="NULL";
}
if($advisor2 <= 0){
$advisor2="NULL";
}
if($webpage != "" && 
!startsWith($webpage,"http://",true) && 
!startsWith($webpage,"https://",true)){
$webpage = "http://".$webpage;
}
if(isset($_POST["notes"])) {
#if(!get_magic_quotes_gpc()) {
	$notes = addslashes($notes);	
#}
//$notes = strip_tags($notes);
// uncomment the following line if you want to allow line breaks
//$notes = str_replace("\n","<br/>", $notes);
}
// Remove punctuations from phone number
$phone = preg_replace('[\D]', '', $phone);

// Let tag be set to NULL
if (!$tag) {
    $tag = "NULL";
} else {
    $tag = "'$tag'";
}

if($_POST["action"] == "Save changes"){
#$query = "UPDATE Person SET ".
#     "Full_name='$name', ".
#     "Title='$title', ".
#     "WhereNow='$wherenow', ".
#     "Advisor=$advisor1, ".
#     "Advisor2=$advisor2, ".
#     "Email='$email', ".
#     "WWW='$webpage', ".
#     "Phone='$phone', ".
#     "Address='$office', ".
#     "Note='$notes', ".
#     "Tag=$tag ".
#     "WHERE uid=$id";
$query = "UPDATE Person SET ".
     "Full_name='$name', ".
     "Title='$title', ".
     "WhereNow='$wherenow', ".
     "Advisor=$advisor1, ".
     "Advisor2=$advisor2, ".
     "Email='$email', ".
     "WWW='$webpage', ".
     "Phone='$phone', ".
     "Address='$office', ".
     "Note='$notes', ".
     "Tag=$tag ".
     "WHERE uid=".mysqli_real_escape_string($db, $id);
#$result = mysql_query($query);
$result = mysqli_query($db,$query);
#if(mysql_error()) {
if(mysqli_error($db)) {
$error = true;
#$ERROR_MSG = mysql_error();
$ERROR_MSG = mysqli_error($db);
} else {
$SUCCESS_MSG = "User information updated successfully!";
}

} else {
$query = "INSERT INTO Person(Full_name,Title,WhereNow,Advisor,Advisor2,Email,WWW,Phone,Address,Note,Tag) VALUES ".
     "('$name','$title','$wherenow',$advisor1,$advisor2,'$email','$webpage','$phone','$office','$notes',$tag);";
#mysql_query($query);
mysqli_query($db,$query);
  #$id = mysql_insert_id();
  $id = mysqli_insert_id($db);
#if(mysql_error()) {
if(mysqli_error($db)) {
$error = true;
#$ERROR_MSG = mysql_error();
$ERROR_MSG = mysqli_error($db);
} else {
$SUCCESS_MSG = "User added successfully!";
}
}

// Update person-area entries. If they are checked but don't exist, insert new entry
// If they are unchecked, but exist, delete that entry
#$result = mysql_query("SELECT * FROM ResearchArea");
$result = mysqli_query($db,"SELECT * FROM ResearchArea");
#while($area = mysql_fetch_assoc($result)) {
while($area = mysqli_fetch_assoc($result)) {
$aid = $area["uid"];
if(isset($_POST["area".$aid])) {
$checked = true;
} else {
$checked = false;
}
#$existing = mysql_num_rows(mysql_query("SELECT * FROM RelPersonArea WHERE PersonID=$id AND AreaID=$aid"));
#$existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelPersonArea WHERE PersonID=$id AND AreaID=$aid"));
$existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelPersonArea WHERE PersonID=".mysqli_real_escape_string($db, $id)." AND AreaID=".mysqli_real_escape_string($db, $aid)));
if($checked==false && $existing==1) {
#mysql_query("DELETE FROM RelPersonArea WHERE PersonID=$id AND AreaID=$aid");
#mysqli_query($db,"DELETE FROM RelPersonArea WHERE PersonID=$id AND AreaID=$aid");
mysqli_query($db,"DELETE FROM RelPersonArea WHERE PersonID=".mysqli_real_escape_string($db, $id)." AND AreaID=".mysqli_real_escape_string($db, $aid));
} else if($checked==true && $existing==0) {
#mysql_query("INSERT INTO RelPersonArea VALUES($id,$aid)"); 
mysqli_query($db,"INSERT INTO RelPersonArea VALUES($id,$aid)"); 
}
#if(mysql_error()) {
if(mysqli_error($db)) {
$error = true;
#$ERROR_MSG = mysql_error();
$ERROR_MSG = mysqli_error($db);
}
}

// Same for projects
#$result = mysql_query("SELECT uid FROM Project");
$result = mysqli_query($db,"SELECT uid FROM Project");
#while($proj = mysql_fetch_assoc($result)) {
while($proj = mysqli_fetch_assoc($result)) {
$projid = $proj["uid"];
if(isset($_POST["project".$projid])) {
$checked = true;
} else {
$checked = false;
}
#$existing = mysql_num_rows(mysql_query("SELECT * FROM RelProjectPerson WHERE PersonID=$id AND ProjectID=$projid"));
#$existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelProjectPerson WHERE PersonID=$id AND ProjectID=$projid"));
$existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelProjectPerson WHERE PersonID=".mysqli_real_escape_string($db, $id)." AND ProjectID=".mysqli_real_escape_string($db, $projid)));
if($checked==false && $existing==1) {
#mysql_query("DELETE FROM RelProjectPerson WHERE PersonID=$id AND ProjectID=$projid");
#mysqli_query($db,"DELETE FROM RelProjectPerson WHERE PersonID=$id AND ProjectID=$projid");
mysqli_query($db,"DELETE FROM RelProjectPerson WHERE PersonID=".mysqli_real_escape_string($db, $id)." AND ProjectID=".mysqli_real_escape_string($db, $projid));
} else if($checked==true && $existing==0) {
#mysql_query("INSERT INTO RelProjectPerson SET PersonID=$id, ProjectID=$projid"); 
mysqli_query($db,"INSERT INTO RelProjectPerson SET PersonID=$id, ProjectID=$projid"); 

}
#if(mysql_error()) {
if(mysqli_error($db)) {
$error = true;
#$ERROR_MSG = mysql_error();
$ERROR_MSG = mysqli_error($db);
}
}

// Same for courses
#$result = mysql_query("SELECT uid FROM Course");
$result = mysqli_query($db,"SELECT uid FROM Course");
#while($proj = mysql_fetch_assoc($result)) {
while($proj = mysqli_fetch_assoc($result)) {
$projid = $proj["uid"];
if(isset($_POST["course".$projid])) {
$checked = true;
} else {
$checked = false;
}
#$existing = mysql_num_rows(mysql_query("SELECT * FROM RelCoursePerson WHERE PersonID=$id AND CourseID=$projid"));
#$existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelCoursePerson WHERE PersonID=$id AND CourseID=$projid"));
$existing = mysqli_num_rows(mysqli_query($db,"SELECT * FROM RelCoursePerson WHERE PersonID=".mysqli_real_escape_string($db, $id)." AND CourseID=".mysqli_real_escape_string($db, $projid)));
if($checked==false && $existing==1) {
#mysql_query("DELETE FROM RelCoursePerson WHERE PersonID=$id AND CourseID=$projid");
#mysqli_query($db,"DELETE FROM RelCoursePerson WHERE PersonID=$id AND CourseID=$projid");
mysqli_query($db,"DELETE FROM RelCoursePerson WHERE PersonID=".mysqli_real_escape_string($db, $id)." AND CourseID=".mysqli_real_escape_string($db, $projid));
} else if($checked==true && $existing==0) {
#mysql_query("INSERT INTO RelCoursePerson SET PersonID=$id, CourseID=$projid"); 
mysqli_query($db,"INSERT INTO RelCoursePerson SET PersonID=$id, CourseID=$projid"); 

}
#if(mysql_error()) {
if(mysqli_error($db)) {
$error = true;
#$ERROR_MSG = mysql_error();
$ERROR_MSG = mysqli_error($db);
}
}

if($_FILES["picture"]["name"]!="") {
//$filename = $_FILES["picture"]["name"];
$tempname = $_FILES["picture"]["tmp_name"];
list($width,$height,$file_type) = getimagesize($tempname);
	
if($width > $height) {
	$biggestside = $width;
	$smallestside = $height;
} else {
	$bigegstside = $height;
$smallestside = $width;
}

$type = exif_imagetype($tempname);
$original_image_gd = null;

if($type==IMAGETYPE_JPEG) { $original_image_gd = imagecreatefromjpeg($tempname); }
else if($type==IMAGETYPE_GIF) { $original_image_gd = imagecreatefromgif($tempname); }
else if($type==IMAGETYPE_PNG) { $original_image_gd = imagecreatefrompng($tempname); }
else { die("Type is ".$type); } 

$cropped_image_gd = imagecreatetruecolor($smallestside,$smallestside);
$wm = $width /$smallestside;
$hm = $height /$smallestside;
$h_height = $smallestside/2;
$w_height = $smallestside/2;

if($width > $height ) {
	$adjusted_width =$width / $hm;
	$half_width = $adjusted_width / 2;
	$int_width = $half_width - $w_height;

	imagecopyresampled($cropped_image_gd ,$original_image_gd ,-$int_width,0,0,0, $adjusted_width, $smallestside, $width , $height );
} else if(($width < $height ) || ($width == $height )) {
	$adjusted_height = $height / $wm;
	$half_height = $adjusted_height / 2;
	$int_height = $half_height - $h_height;

	imagecopyresampled($cropped_image_gd , $original_image_gd ,0,-$int_height,0,0, $smallestside, $adjusted_height, $width , $height );
} else {
	$cropped_image_gd = $original_image_gd;
} 

$resized_image_gd = imagecreatetruecolor(500,500);
imagecopyresampled($resized_image_gd, $cropped_image_gd, 0, 0, 0, 0, 500, 500, $smallestside, $smallestside);
imagejpeg($resized_image_gd,$tempname);
imagedestroy($original_image_gd);
imagedestroy($cropped_image_gd);
imagedestroy($resized_image_gd);
	
$fp = fopen($tempname, "r");
$content = fread($fp, filesize($tempname));
$content = addslashes($content);
fclose($fp);
#$updatePdf_query = "UPDATE Person SET Picture='$content' WHERE uid=$id";
$updatePdf_query = "UPDATE Person SET Picture='$content' WHERE uid=".mysqli_real_escape_string($db, $id);
#mysql_query($updatePdf_query);
mysqli_query($db,$updatePdf_query);
#if(mysql_error()) {
if(mysqli_error($db)) {
    $error = true;
    #$ERROR_MSG = mysql_error();
    $ERROR_MSG = mysqli_error($db);
}
}

}

#$result = mysql_query("SELECT * from Person where uid=$id;");
$result = mysqli_query($db,"SELECT * from Person where uid=$id;");

#if($result && mysql_num_rows($result)>0) {
if($result && mysqli_num_rows($result)>0) {
#$user = mysql_fetch_assoc($result);
$user = mysqli_fetch_assoc($result);
$name = $user["Full_name"];
$title = $user["Title"];
$wherenow = $user["WhereNow"];
$advisor1 = $user["Advisor"];
$advisor2 = $user["Advisor2"];
$disableprofile = $user["DisableProfile"];
$email = $user["Email"];
$webpage = $user["WWW"];
$phone = $user["Phone"];
$office = $user["Address"];
$notes = $user["Note"];
$picture = $user["Picture"];
$tag = $user["Tag"];
$action = "Save changes";
$label = "edit";
} else {
$id = "";
$action = "Add user";
$label = "new";
}

$s3facultylist = array();
$advisor2list= array();

#$result = mysql_query("SELECT uid, Full_name, Email from Person where Title='faculty' or Title='affilfaculty';");
$result = mysqli_query($db,"SELECT uid, Full_name, Email from Person where Title='faculty' or Title='affilfaculty';");
$index = 0;
#while( $row = mysql_fetch_assoc($result) ){
while( $row = mysqli_fetch_assoc($result) ){
$s3facultylist[$index]["name"] = $row["Full_name"];
$s3facultylist[$index]["email"] = $row["Email"];
$s3facultylist[$index]["uid"] = $row["uid"];
$advisor2list[$index]["name"] = $row["Full_name"];
$advisor2list[$index]["email"] = $row["Email"];
$advisor2list[$index]["uid"] = $row["uid"];
$index++;
}

#$result = mysql_query("SELECT uid, Full_name, Email from Person where Title='Non-S3';");
$result = mysqli_query($db,"SELECT uid, Full_name, Email from Person where Title='ext';");
#while( $row = mysql_fetch_assoc($result) ){
while( $row = mysqli_fetch_assoc($result) ){
$advisor2list[$index]["name"] = $row["Full_name"];
$advisor2list[$index]["email"] = $row["Email"];
$advisor2list[$index]["uid"] = $row["uid"];
$index++;
}
?>

<html>
<head>
<title>S3 Lab - Software & Systems Security Laboratory</title>
<link href="../global.css" rel="stylesheet" type="text/css">
<link href="internal-form.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function setAdvisorDisplay() {
	var value = document.getElementById("person-title").value;
	//alert(value);
	if(value == "Faculty" || value == "Research staff" || value == "Staff" || value == "Alumni") {
		document.getElementById("advisor1").disabled = true;
		document.getElementById("advisor2").disabled = true;
	} else {
		document.getElementById("advisor1").disabled = false;
		document.getElementById("advisor2").disabled = false;
	}	
}
</script>

<?php INCLUDE_TINYMCE() ?>
</head>

<body onload="setAdvisorDisplay();">
<div id="internal-form-page" class="page-container">

<div id="title">
<a href=".."><img src="../pics/title.png" style="width:100%"></a>
</div>

<div id="menu-stack">
::
<a href="index.php">maintenance</a>
&rsaquo;&rsaquo;
<a href="show_users.php">all users</a>
&rsaquo;&rsaquo;
</div>

<div id="navigation">
view all
<a href="show_news.php">news</a>
::
<a href="show_areas.php">areas</a>
::
<a href="show_pubs.php">publications</a>
::
<a href="show_projects.php">projects</a>
::
<a href="show_courses.php">courses</a>
</div>

<?php
if($error) {
printErrorFrame($ERROR_MSG);
} else if(isset($SUCCESS_MSG)) {
printSuccessFrame($SUCCESS_MSG);
}
?>

<div id="form">
<p class="section-title"><?php echo $label; ?> user</p>

<p>
<?php
$action_suffix = ($id!=""?"?uid=".$id:"");
?>
<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']).$action_suffix;?>" enctype="multipart/form-data">
<input type=hidden name="user-id" value="<?php echo $id; ?>">
<input type=hidden id="action" name="action" value="<?php echo $action; ?>">

<table border=0>
<tr>
<td class="single-row-header">Full name</td>
<td>
<input type="text" name="name" class="long-text-input" 
value="<?php echo $name;?>" />
</td>
</tr>

<tr>
<td class="single-row-header">Title</td>
<td>
<select name="title" id="person-title" onChange="setAdvisorDisplay();">
<?php
$map = array("faculty"=>"Faculty",
     "affilfaculty" => "Affiliated faculty",
     "phd" => "PhD student",
     "masters" => "Masters student",
     "undergrad" => "Undergraduate student",
     "postdoc" => "Post-doctoral associate",
     "scientist" => "Research staff",
     "visitor" => "Visiting scholar",
     "staff" => "Staff",
     "alum" => "Alumni",
     "ext" => "External collaborator",
     "other" => "Other");
foreach ($map as $dbEnumName => $MenuName){
if($title == $dbEnumName){
echo '<option value="'.$dbEnumName.'" selected="yes">'.$MenuName.'</option>'."\n";
} else {
echo '<option value="'.$dbEnumName.'">'.$MenuName.'</option>'."\n";
}
}
?>
</select>
</tr>

<tr>
<td>
Areas
</td>
<td>

<?php
$pid = ($id!=""?$id:-1);
getPersonAreaTable($db,$pid);
?>

</td>
</tr>

<tr>
<td>
Projects
</td>
<td>

<?php
$pid = ($id!=""?$id:-1);
getProjTable($db,$pid,"Person");
?>

</td>
</tr>

<tr>
<td>
Courses
</td>
<td>

<?php
$pid = ($id!=""?$id:-1);
getCourseTable($db,$pid,"Person");
?>

</td>
</tr>

<tr>
<td class="multi-row-header">Advisors</td>
<td>
<select name="advisor1" id="advisor1" disabled>
<option value="-1">Primary advisor</option>
<?php 
for ($i = 0; $i < sizeof($s3facultylist); $i++){
echo '<option value="'.$s3facultylist[$i]["uid"].'"';
if($advisor1 == $s3facultylist[$i]["uid"]) echo 'selected';
echo '>'.$s3facultylist[$i]["name"].' ('.$s3facultylist[$i]["email"].')</option>'."\n";
}
?>
<!-- 
<option value="1">Lorenzo Alvisi (lorenzo@cs.utexas.edu)</option>
<option value="2">Mike Dahlin (dahlin@cs.utexas.edu)</option>
-->
</select>
<br />

<select name="advisor2" id="advisor2" disabled>
<option value="-1">Second advisor (optional)</option>
<?php 
for ($i = 0; $i < sizeof($advisor2list); $i++){
echo '<option value="'.$advisor2list[$i]["uid"].'"';
if($advisor2 == $advisor2list[$i]["uid"]) echo 'selected';
echo '>'.$advisor2list[$i]["name"].' ('.$advisor2list[$i]["email"].')</option>'."\n";
}
?>
<!--
<option value="3">Vitaly Shmatikov (shmat@cs.utexas.edu)</option>
-->
</select>
</tr>

<tr>
<td class="single-row-header">Email</td>
<td>
<input type="text" name="email" class="long-text-input" 
value="<?php echo $email;?>"/>
</td>
</tr>

<tr>
<td class="single-row-header">Webpage</td>
<td>
<input type="text" name="webpage" class="long-text-input" 
value="<?php echo $webpage;?>"/>
</td>
</tr>

<tr>
<td class="single-row-header">Phone (optional)</td>
<td>
<input type="text" name="phone" class="long-text-input"
value="<?php echo $phone;?>" />
</td>
</tr>

<tr>
<td class="single-row-header">Office (optional)</td>
<td>
<input type="text" name="office" class="long-text-input"
value="<?php echo $office;?>"/>
</td>
</tr>

<tr>
<!--<td class="single-row-header">Where now (alumni only)</td>-->
<td class="single-row-header">First employment (alumni only)</td>
<td>
<input type="text" name="wherenow" class="long-text-input"
value="<?php echo $wherenow;?>"/>
</td>
</tr>

<tr>
<td class="single-row-header">Username (optional, must be unique)</td>
<td>
<input type="text" name="tag" class="long-text-input"
value="<?php echo $tag;?>"/>
</td>
</tr>

<tr>
<td class="multi-row-header">Notes (optional)</td>
<td>
<textarea name="notes" class="textarea">
<?php echo $notes ?>
</textarea><br/>
</td>
</tr>

<tr>
<td class="multi-row-header">
Picture (optional)<br/>
</td>
<td>
<input type="file" name="picture" value="" ><br/>
<span style="font-size:9pt">
Please ensure that (a) the picture is at least 500x500 and (b) is square; if it isn't, the script will make it square, and you may not like the results.
</span>
</td>
</tr>

<?php
if($picture != ""){
echo '<tr>'."\n";
echo '<td></td>'."\n";
echo '<td class="single-row-header">'."\n";
echo '<img src="../download.php?uid='.$id.'&picture=1&'.rand().'" alt="" height="100" width="100">'."\n";
echo '</td>'."\n";
echo '</tr>'."\n";
}
?>
<tr>
<td>
</td>
<td>
<input type="submit" name="submit" value="<?php echo $action; ?>" />
</td>
</tr>

</table>

</form>

</div>

</div>
</body>
</html>

