<?php
include 'header.inc';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
include '../tools.inc';
error_reporting(E_ALL);
ini_set('display_errors', 1);

$error = false;
if(isset($_GET["deleteNews"])) {
        $delId = $_GET["deleteNews"];
        #$delete_query = "DELETE FROM News WHERE uid = $delId";
        $delete_query = "DELETE FROM News WHERE uid = ".mysqli_real_escape_string($db, $delId);
        #mysql_query($delete_query);
        mysqli_query($db,$delete_query);
        #if(mysql_error()) {
        if(mysqli_error($db)) {
                $error = true;
                #$ERROR_MSG = mysql_error();
                $ERROR_MSG = mysqli_error($db);
        } else {
            $SUCCESS_MSG = "News deleted successfully!";
        }
}

#$result = mysql_query("SELECT uid, Date, Description from News ORDER BY date DESC, uid DESC;");
$result = mysqli_query($db,"SELECT uid, Date, Description from News ORDER BY Date DESC, uid DESC;");
?>

<html>
<head>
    <title>S3 Lab - Software & Systems Security Laboratory</title>
    <link href="../global.css" rel="stylesheet" type="text/css">
    <link href="internal-listing.css" rel="stylesheet" type="text/css">
</head>

<div id="internal-listing-page" class="page-container">

<div id="title">
<a href=".."><img src="../pics/title.png" style="width:100%"></a>
</div>

<div id="menu-stack">
::
<a href="index.php">maintenance</a>
&rsaquo;&rsaquo;
</div>

<div id="navigation">
view all
<a href="show_areas.php">areas</a>
::
<a href="show_users.php">users</a>
::
<a href="show_pubs.php">publications</a>
::
<a href="show_projects.php">projects</a>
::
<a href="show_courses.php">courses</a>
::
<a href="show_sponsors.php">sponsors</a>
</div>

<?php
if($error) {
        printErrorFrame($ERROR_MSG);
} else if(isset($SUCCESS_MSG)) {
        printSuccessFrame($SUCCESS_MSG);
}
?>

<div class="listing box-shadow">
<p class="section-title">news list</p>

<p>
Click on "edit" or "delete" next to the news to modify the news.
<a href="news.php">Click here</a> to add a news.
</p>

<p>
<form>
<table>
<tr>
<td class="col-header">Date</td>
<td class="col-header">Description</td>
</tr>

<?php

$count = 0;
#while($news = mysql_fetch_assoc($result)) {
while($news = mysqli_fetch_assoc($result)) {
	$count++;
	if($count%2 == 1) {
		echo "<tr class = \"odd-row\">\n";
	} else {
		echo "<tr class = \"even-row\">\n";
	}

	echo "<td class=\"publications\">\n";
	echo "  <span class=\"title\"><a href=\"news.php?uid=".$news["uid"]."\" class=\"primary-action\">".date("m/d/Y", strtotime($news["Date"]))."</a></span><br>\n";
	echo " [<a href=\"news.php?uid=".$news["uid"]."\" class=\"primary-action\">edit</a> :: ";
	echo "<a href=\"".htmlentities($_SERVER['PHP_SELF'])."?deleteNews=".$news["uid"]."\" class=\"primary-action\">delete</a>]<br/>\n";
	echo "</td>\n";

	echo "<td class=\"data\">\n";
	echo "$news[Description]";
        echo "</td>\n";
}
?>
</table>

</form>

</div>

</div>
</html>

