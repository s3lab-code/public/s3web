<?php

include '../config.inc';

session_start();
$redirect = false;
$suffix = "";
if(isset($_SESSION["username"])) {
	#$db = mysql_connect($DATABASE_HOST,$_SESSION["username"],$_SESSION["password"]);
	$db = mysqli_connect($DATABASE_HOST,$_SESSION["username"],$_SESSION["password"],$DATABASE_NAME);
	if(!$db) {
		$ERROR_MSG="Invalid credentials, please try again.";
		$redirect = true;
		$suffix = "?message=invalid";
	#} else {
	#	$flag = mysql_select_db($DATABASE_NAME, $db);
	#	if($flag == false) {
	#		$redirect = true;
	#	}
	}
} else {
	$redirect = true;
}

if($redirect == true) {
?>
<html>
<head>
<meta HTTP-EQUIV="REFRESH" content="0; url=login.php<?php echo $suffix;?>">
</head>
</html>
<?php exit();
}

function INCLUDE_TINYMCE($where = "textarea") {
?>
<!-- TinyMCE -->
<script type="text/javascript" src="../tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
        selector: "<?php echo $where; ?>",
        plugins: "code lists link image charmap hr searchreplace fullscreen table contextmenu paste"
             });
</script>

<?php
}
?>

