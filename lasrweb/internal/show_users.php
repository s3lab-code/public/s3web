<?php
include 'header.inc';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
include '../tools.inc';

$error = false;
if(isset($_GET["deleteUser"])) {
        $delId = $_GET["deleteUser"];
	#$pubs = mysql_query("SELECT PaperID FROM RelPaperAuthor WHERE AuthorID=$delId");
	#$pubs = mysqli_query($db,"SELECT PaperID FROM RelPaperAuthor WHERE AuthorID=$delId");
	$pubs = mysqli_query($db,"SELECT PaperID FROM RelPaperAuthor WHERE AuthorID=".mysqli_real_escape_string($db, $delId));
	#if(mysql_num_rows($pubs)==0) {
	if(mysqli_num_rows($pubs)==0) {
        	#$delete_query = "DELETE FROM Person WHERE uid = $delId";
        	$delete_query = "DELETE FROM Person WHERE uid = ".mysqli_real_escape_string($db, $delId);
        	#mysql_query($delete_query);
        	mysqli_query($db,$delete_query);
        	#if(mysql_error()) {
        	if(mysqli_error($db)) {
        	    $error = true;
        	    #$ERROR_MSG = mysql_error();
        	    $ERROR_MSG = mysqli_error($db);
        	} else {
                #$delete_paper_query = "DELETE FROM RelPaperAuthor WHERE AuthorID = $delId";
                $delete_paper_query = "DELETE FROM RelPaperAuthor WHERE AuthorID = ".mysqli_real_escape_string($db, $delId);
                #mysql_query($delete_paper_query);
                mysqli_query($db,$delete_paper_query);
                #if(mysql_error()) {
                if(mysqli_error($db)) {
                    $error = true;
                    #$ERROR_MSG = mysql_error();
                    $ERROR_MSG = mysqli_error($db);
                } else {
                    #$delete_project_query = "DELETE FROM RelProjectPerson WHERE PersonID = $delId";
                    $delete_project_query = "DELETE FROM RelProjectPerson WHERE PersonID = ".mysqli_real_escape_string($db, $delId);
                    #mysql_query($delete_project_query);
                    mysqli_query($db,$delete_project_query);
                    #if(mysql_error()) {
                    if(mysqli_error($db)) {
                        $error = true;
                        #$ERROR_MSG = mysql_error();
                        $ERROR_MSG = mysqli_error($db);
                    } else {
                        #$delete_course_query = "DELETE FROM RelCoursePerson WHERE PersonID = $delId";
                        $delete_course_query = "DELETE FROM RelCoursePerson WHERE PersonID = ".mysqli_real_escape_string($db, $delId);
                        #mysql_query($delete_course_query);
                        mysqli_query($db,$delete_course_query);
                        #if(mysql_error()) {
                        if(mysqli_error($db)) {
                            $error = true;
                            #$ERROR_MSG = mysql_error();
                            $ERROR_MSG = mysqli_error($db);
                        } else {
            	            $SUCCESS_MSG = "User deleted successfully!";
                        }
                    }
                }
        	}
	} else {
		$error = true;
		#$suffix = mysql_num_rows($pubs)>1?"s":"";
		$suffix = mysqli_num_rows($pubs)>1?"s":"";
		#$ERROR_MSG = "Delete failed. User is a coauthor of ".mysql_num_rows($pubs)." publication".$suffix;
		$ERROR_MSG = "Delete failed. User is a coauthor of ".mysqli_num_rows($pubs)." publication".$suffix;
	}
}
?>

<html>
<head>
    <title>S3 Lab - Software & Systems Security Laboratory</title>
    <link href="../global.css" rel="stylesheet" type="text/css">
    <link href="../listing.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="listing-page" class="page-container">

<div id="title">
<a href=".."><img src="../pics/title.png" style="width:100%"></a>
</div>

<div id="menu-stack">
::
<a href="index.php">maintenance</a>
&rsaquo;&rsaquo;
</div>

<div id="navigation">
view all
<a href="show_news.php">news</a>
::
<a href="show_areas.php">areas</a>
::
<a href="show_pubs.php">publications</a>
::
<a href="show_projects.php">projects</a>
::
<a href="show_courses.php">courses</a>
::
<a href="show_sponsors.php">sponsors</a>
</div>

<?php
if($error) {
        printErrorFrame($ERROR_MSG);
} else if(isset($SUCCESS_MSG)) {
        printSuccessFrame($SUCCESS_MSG);
}
?>

<div id="listing" class="box-shadow people-listing">

<p class="section-title">
all users
</p>

<p>
To add a new user, <a href="user.php" class="small-link">click here</a>.  
</p>


<div style="clear:both;">

<?php 
  
$positions_to_show = array( "faculty", "affilfaculty", "scientist", "postdoc", "phd", "masters", "undergrad", "staff", "alum", "visitor", "ext", "other" );

foreach ($positions_to_show as $position){

  #$query = "SELECT uid, Full_name, Title, Picture FROM Person "
  #      ."WHERE Title='".$position."' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1);";
  $query = "SELECT uid, Full_name, Title, Picture FROM Person "
        ."WHERE Title='".mysqli_real_escape_string($db, $position)."' ORDER BY SUBSTRING_INDEX(Full_name,' ',-1);";

  #$result = mysql_query($query);
  $result = mysqli_query($db,$query);

  #while($row = mysql_fetch_assoc($result)){
  while($row = mysqli_fetch_assoc($result)){
    $pic = $row["Picture"];
    if($pic == null) {
	$src = "../pics/nobody.png";
    } else {
	$src = "../download.php?uid=".$row["uid"]."&picture=1";
    }
    /*if ( file_exists($pic_path) ){
        $dotPos = strrpos($pic_path, ".");
        if($dotPos != False){
            $pic_path = substr($pic_path,0,$dotPos)."-small".substr($pic_path,$dotPos);
        } else {
            $pic_path = $pic_path."-small";
        }
    } else {
      $pic_path = "../people_pics/nobody.jpg";
    }
    if( !file_exists($pic_path) ){
      $pic_path = "../people_pics/nobody.jpg";
    }*/

    echo '<div class="person-profile">'."\n";
    echo '<a href="user.php?uid='.$row['uid'].'" class="name"><img src="'.$src.'" style="width:100%"></a>'."<br/>\n";
    echo '<a href="user.php?uid='.$row['uid'].'" class="name">'.$row['Full_name'].'</a><br/>'."\n";
    echo '<span class="position">'.getPosition($position)."</span><br/>\n";
    echo '<a href="user.php?uid='.$row['uid'].'">edit</a> :: ';
    echo "<a href=\"".htmlentities($_SERVER['PHP_SELF'])."?deleteUser=".$row["uid"]."\">delete</a>\n";
    echo '</div>'."\n";
  }
}

?>

<div style="clear:both;"> </div>

</div>
</div>

</body>
</html>


