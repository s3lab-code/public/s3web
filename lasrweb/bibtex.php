<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php 
require "header.inc";
error_reporting(E_ALL);
ini_set('display_errors', '1');

$id = filter_input(INPUT_GET, "uid", FILTER_VALIDATE_INT);
if($id == NULL && $id == FALSE) {
        die("Invalid unique id");
}

#$result = mysql_query("SELECT * from Paper where uid=$id;");
$result = mysqli_query($db,"SELECT * from Paper where uid=$id;");
#if(!$result || mysql_num_rows($result) == 0) {
if(!$result || mysqli_num_rows($result) == 0) {
	die("Invalid unique id");
}

#$paper = mysql_fetch_assoc($result);
$paper = mysqli_fetch_assoc($result);

if ($paper["BibEntry"]) {
    $hypertext = "<pre>" . $paper["BibEntry"] . "</pre>";
} else {

$list = $paper["AuthorList"];
$authors = explode(",", $list);
$authorString = "";
for($i=0;$i < count($authors); $i++) {
        if (is_numeric($authors[$i])) {
            #$res = mysql_query("SELECT Full_name FROM Person WHERE uid=$authors[$i];");
            #$res = mysqli_query($db,"SELECT Full_name FROM Person WHERE uid=$authors[$i];");
            $res = mysqli_query($db,"SELECT Full_name FROM Person WHERE uid=".mysqli_real_escape_string($db, $authors[$i]).";");
            #$author = mysql_fetch_assoc($res);
            $author = mysqli_fetch_assoc($res);
            $authname = $author["Full_name"];
        } else {
            $authname = $authors[$i];
        }
	if($i != 0) {
		$authorString .= " and ";
	} else {
		$firstAuthorString = $authname;
	}
	$authorString .= $authname;
}
$venueYear = $paper["Year"];
$shortName = $paper["Venue_short"];
$type = $paper["Type"];
$doi = $paper["DOI"];

switch($type) {
case 'Conference':
case 'Workshop':
	$pubtype="inproceedings"; 
	break;
case 'Journal':
case 'Magazine':
	$pubtype="article"; 
	break;
case 'TR':
	$pubtype="TechReport"; 
	break;
case 'MSThesis':
        $pubtype = "MSThesis";
        break;
case 'PHDThesis':
        $pubtype = "PHDThesis";
        break;
case 'Other':
	$pubtype="misc"; 
	break;
default:
	break;
}

$firstauthorsplit=explode(" ",$firstAuthorString);
$firstauthorsurname=end($firstauthorsplit);
$yearstr=substr(strval($venueYear),-2);
$words=explode(" ",$paper["Title"]);
$firstWord=explode(":",$words[0]);
$firstWord[0]=str_replace("&#960;","pi",$firstWord[0]);
$firstFirstWordNonStripped=$firstWord[0];
$firstFirstWord=preg_replace("/[^A-Za-z]/", "", $firstFirstWordNonStripped);

//$firstWord=explode(":",explode(" ",$paper["Title"])[0])[0];
$span = '<span style="padding-left:50px">';
$paperID = $firstauthorsurname . $yearstr . $firstFirstWord;

$parsedTitle = "";
foreach ($words as $word) {
	$numUpperCase = preg_match_all('/[A-Z]/',$word,$matches);
	if($numUpperCase > 1) {
		$parsedTitle .= "{".$word."} ";
	} else {
		$parsedTitle .= $word." ";
	}
}
$parsedTitle = substr($parsedTitle,0,-1);
$hypertext  = "@" . $pubtype . "{<br>\n";
$hypertext .= "\t" . $span . $paperID . ",</span><br>\n";
$hypertext .= "\t" . $span . "Author = {" . $authorString . "},</span><br>\n";
$hypertext .= "\t" . $span . "Title = {" . $parsedTitle . "},</span><br>\n";
if($type == "Conference") {
	$hypertext .= "\t" . $span . "Booktitle = {" . $shortName . "},</span><br>\n";
} else if ($type == "Journal") {
	$hypertext .= "\t" . $span . "Journal = {" . $shortName . "},</span><br>\n";
} else if ($type == "MSThesis" || $type = "PHDThesis") {
        $hypertext .= "\t" . $span . "School = {" . $paper["Institution"] . "},</span><br>\n";
}
if ($doi) {
    $hypertext .= "\t" . $span . "DOI = {" . $doi . "},</span><br>\n";
}
$hypertext .= "\t" . $span . "Year = {" . $venueYear . "}</span><br>\n";
$hypertext .= "}";
}

?>
<html>
<body>
<tt><?php
echo $hypertext;
?></tt>
</body>
</html>
