-- MySQL dump 10.13  Distrib 5.1.62, for portbld-freebsd8.2 (amd64)
--
-- Host: localhost    Database: lasrweb_beta
-- ------------------------------------------------------
-- Server version	5.1.62

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Paper`
--

DROP TABLE IF EXISTS `Paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Paper` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL DEFAULT '',
  `VenueID` int(11) NOT NULL DEFAULT '0',
  `AuthorList` varchar(255) DEFAULT NULL,
  `PageNum` varchar(15) DEFAULT NULL,
  `Pdf` mediumblob,
  `Abstract` text,
  `InReview` tinyint(1) DEFAULT '0',
  `Thumbnail` mediumblob,
  `slides` mediumblob,
  `slides_type` varchar(10) DEFAULT NULL,
  `AwardPaper` tinyint(1) DEFAULT '0',
  `BibEntry` text,
  `Venue_full` varchar(255) DEFAULT NULL,
  `Venue_short` varchar(20) NOT NULL,
  `Type` enum('Conference','Journal','Workshop','TR','Magazine','MSThesis','PHDThesis','Other') DEFAULT NULL,
  `Year` int(11) DEFAULT NULL,
  `Location` varchar(50) DEFAULT NULL,
  `VolumeNum` varchar(50) DEFAULT NULL,
  `Number` varchar(50) DEFAULT NULL,
  `Month` int(11) DEFAULT NULL,
  `DOI` varchar(255) DEFAULT NULL,
  `Copyright` varchar(1024) DEFAULT NULL,
  `ArtType` varchar(50) DEFAULT NULL,
  `Institution` varchar(255) DEFAULT NULL,
  `Code` varchar(512) DEFAULT NULL,
  `Web` varchar(512) DEFAULT NULL,
  `LastMod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `Tag` (`Tag`),
  KEY `VenueID` (`VenueID`)
) ENGINE=MyISAM AUTO_INCREMENT=177 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Person`
--

DROP TABLE IF EXISTS `Person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Person` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Full_name` varchar(100) NOT NULL DEFAULT '',
  `Title` enum('faculty','affilfaculty','scientist','postdoc','phd','masters','undergrad','staff','alum','visitor','ext','other') DEFAULT NULL,
  `DisableProfile` tinyint(1) NOT NULL DEFAULT '0',
  `Email` varchar(100) DEFAULT NULL,
  `WWW` varchar(255) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `Note` text,
  `Picture` mediumblob,
  `Advisor` int(11) DEFAULT NULL,
  `Advisor2` int(11) DEFAULT NULL,
  `WhereNow` varchar(255) DEFAULT NULL,
  `LastMod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `Tag` (`Tag`),
  KEY `Advisor` (`Advisor`),
  KEY `Advisor2` (`Advisor2`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Project`
--

DROP TABLE IF EXISTS `Project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Project` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Description` text,
  `Details` text,
  `Private` text,
  `Picture` mediumblob,
  `LastMod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `Name` (`Name`),
  UNIQUE KEY `Tag` (`Tag`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Course`
--

DROP TABLE IF EXISTS `Course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Course` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Title` varchar(255) NOT NULL DEFAULT '',
  `Description` text,
  `Details` text,
  `Picture` mediumblob,
  `LastMod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `Name` (`Name`),
  UNIQUE KEY `Tag` (`Tag`)
) ENGINE=MyISAM AUTO_INCREMENT=401 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RelPaperArea`
--

DROP TABLE IF EXISTS `RelPaperArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelPaperArea` (
  `PaperID` int(11) NOT NULL DEFAULT '0',
  `AID` int(11) NOT NULL DEFAULT '0',
  KEY `PaperID` (`PaperID`),
  KEY `AID` (`AID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RelPaperAuthor`
--

DROP TABLE IF EXISTS `RelPaperAuthor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelPaperAuthor` (
  `PaperID` int(11) NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  KEY `PaperID` (`PaperID`),
  KEY `AuthorID` (`AuthorID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RelPersonArea`
--

DROP TABLE IF EXISTS `RelPersonArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelPersonArea` (
  `PersonID` int(11) NOT NULL DEFAULT '0',
  `AreaID` int(11) NOT NULL DEFAULT '0',
  KEY `PersonID` (`PersonID`),
  KEY `AreaID` (`AreaID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RelProjectArea`
--

DROP TABLE IF EXISTS `RelProjectArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelProjectArea` (
  `ProjectID` int(11) NOT NULL DEFAULT '0',
  `AID` int(11) NOT NULL,
  KEY `ProjectID` (`ProjectID`),
  KEY `AreaID` (`AID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RelProjectPaper`
--

DROP TABLE IF EXISTS `RelProjectPaper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelProjectPaper` (
  `ProjectID` int(11) NOT NULL DEFAULT '0',
  `PaperID` int(11) NOT NULL DEFAULT '0',
  KEY `ProjectID` (`ProjectID`),
  KEY `PaperID` (`PaperID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RelProjectPerson`
--

DROP TABLE IF EXISTS `RelProjectPerson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelProjectPerson` (
  `ProjectID` int(11) NOT NULL DEFAULT '0',
  `PersonID` int(11) NOT NULL DEFAULT '0',
  KEY `ProjectID` (`ProjectID`),
  KEY `PersonID` (`PersonID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RelProjectSponsor`
--

DROP TABLE IF EXISTS `RelProjectSponsor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelProjectSponsor` (
  `ProjectID` int(11) NOT NULL DEFAULT '0',
  `SponsorID` int(11) NOT NULL DEFAULT '0',
  KEY `ProjectID` (`ProjectID`),
  KEY `SponsorID` (`SponsorID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RelCourseArea`
--

DROP TABLE IF EXISTS `RelCourseArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelCourseArea` (
  `CourseID` int(11) NOT NULL DEFAULT '0',
  `AID` int(11) NOT NULL,
  KEY `CourseID` (`CourseID`),
  KEY `AreaID` (`AID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RelCoursePerson`
--

DROP TABLE IF EXISTS `RelCoursePerson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelCoursePerson` (
  `CourseID` int(11) NOT NULL DEFAULT '0',
  `PersonID` int(11) NOT NULL DEFAULT '0',
  KEY `CourseID` (`CourseID`),
  KEY `PersonID` (`PersonID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ResearchArea`
--

DROP TABLE IF EXISTS `ResearchArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ResearchArea` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `Name` (`Name`),
  UNIQUE KEY `Tag` (`Tag`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VenueGeneric`
--

DROP TABLE IF EXISTS `VenueGeneric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VenueGeneric` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Full_name` varchar(100) NOT NULL DEFAULT '',
  `Short_name` varchar(20) NOT NULL DEFAULT '',
  `Type` enum('Conference','Journal','Workshop','TR','Magazine','Other') NOT NULL DEFAULT 'Conference',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `Full_name` (`Full_name`),
  UNIQUE KEY `Short_name` (`Short_name`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VenueSpecific`
--

DROP TABLE IF EXISTS `VenueSpecific`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VenueSpecific` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Venue` int(11) NOT NULL DEFAULT '0',
  `Year` int(11) NOT NULL DEFAULT '0',
  `Location` varchar(50) DEFAULT NULL,
  `VolumeNum` int(11) DEFAULT NULL,
  `Number` int(11) DEFAULT NULL,
  `Month` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `genric_year` (`Venue`,`Year`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Table structure for table `News`
--

DROP TABLE IF EXISTS `News`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `News` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `Description` text,
  `LastMod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=501 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Sponsor`
--

DROP TABLE IF EXISTS `Sponsor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sponsor` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `WWW` varchar(255) DEFAULT NULL,
  `Picture` mediumblob,
  `LastMod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `Name` (`Name`),
  UNIQUE KEY `Tag` (`Tag`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-02  4:26:05
